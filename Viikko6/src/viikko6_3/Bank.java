package viikko6_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public abstract class Bank {
	private BufferedReader br = new BufferedReader(
			new InputStreamReader(System.in));
	
	protected String tilinumero;
	protected String rahamäärä;
	protected String luottoraja;
	
	public void lisääTili() {
		tilinumero = null;
		rahamäärä = null;
		System.out.print("Syötä tilinumero: ");
		tilinumero = inputString(tilinumero);
		System.out.print("Syötä rahamäärä: ");
		rahamäärä = inputString(rahamäärä);
		System.out.println("Pankkiin lisätään: " + tilinumero + "," + rahamäärä);
	}
	
	public void lisääLuottoTili() {
		tilinumero = null;
		rahamäärä = null;
		luottoraja = null;
		System.out.print("Syötä tilinumero: ");
		tilinumero = inputString(tilinumero);
		System.out.print("Syötä rahamäärä: ");
		rahamäärä = inputString(rahamäärä);
		System.out.print("Syötä luottoraja: ");
		luottoraja = inputString(luottoraja);
		System.out.println("Pankkiin lisätään: " + tilinumero + "," + rahamäärä + "," + luottoraja);
	}
	
	public void tallennaRahaa() {
		tilinumero = null;
		rahamäärä = null;
		System.out.print("Syötä tilinumero: ");
		tilinumero = inputString(tilinumero);
		System.out.print("Syötä rahamäärä: ");
		rahamäärä = inputString(rahamäärä);
		System.out.println("Talletetaan tilille: " + tilinumero + " rahaa " + rahamäärä);
	}
	
	public void nostaRahaa() {
		tilinumero = null;
		rahamäärä = null;
		System.out.print("Syötä tilinumero: ");
		tilinumero = inputString(tilinumero);
		System.out.print("Syötä rahamäärä: ");
		rahamäärä = inputString(rahamäärä);
		System.out.println("Nostetaan tililtä: " + tilinumero + " rahaa " + rahamäärä);
	}
	
	public void poistaTili() {
		tilinumero = null;
		System.out.print("Syötä poistettava tilinumero: ");
		tilinumero = inputString(tilinumero);
		System.out.println("Tili poistettu.");
	}
	
	public void tulostaTili() {
		tilinumero = null;
		System.out.print("Syötä tulostettava tilinumero: ");
		tilinumero = inputString(tilinumero);
		System.out.println("Etsitään tiliä: " + tilinumero);
	}
	
	public void tulostaKaikki() {
		System.out.println("Kaikki tilit:");
	}

	public String inputString(String muuttuja) {
		try {
			muuttuja = br.readLine();
		} catch (IOException ex) {
		}
		return muuttuja;
	}
}

class Pankki extends Bank {
	public Pankki() {
	}
}



