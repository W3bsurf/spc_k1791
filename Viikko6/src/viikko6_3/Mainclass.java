package viikko6_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Mainclass {
	private static BufferedReader br = new BufferedReader(
			new InputStreamReader(System.in));
	private static Bank pankki;
	
	public static void main(String[] args) {
		int valinta;
		pankki = new Pankki();
		while (true) {
			valinta = valikko(0, 7);
			if (valinta == 1) {
				pankki.lisääTili();
			} else if (valinta == 2) {
				pankki.lisääLuottoTili();
			} else if (valinta == 3) {
				pankki.tallennaRahaa();
			} else if (valinta == 4) {
				pankki.nostaRahaa();
			} else if (valinta == 5) {
				pankki.poistaTili();
			} else if (valinta == 6) {
				pankki.tulostaTili();
			} else if (valinta == 7) {
				pankki.tulostaKaikki();
			} else if (valinta == 0) {
				break;
			}
		}
	}
	
	public static int valikko(int ala, int ylä) {
		int valinta;
		
		while (true) {
			System.out.print("\n*** PANKKIJÄRJESTELMÄ ***\n1) Lisää tavallinen tili\n2)" +
					" Lisää luotollinen tili\n3) Tallenna tilille rahaa\n4) Nosta tililtä\n" +
					"5) Poista tili\n6) Tulosta tili\n7) Tulosta kaikki tilit\n0) Lopeta\n");
			System.out.print("Valintasi: ");
			try {
				try {
					valinta = Integer.parseInt(br.readLine());
					if (valinta < ala || valinta > ylä) {
						System.out.println("Valintasi ei kelpaa.");
					}
					else {
						return valinta;
					}
				} catch (NumberFormatException ex) {
					System.out.println("Valinta ei kelpaa.");
				}
			} catch (IOException ex) {
			}
		}
	}
}
