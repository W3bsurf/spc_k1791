package viikko6_4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Mainclass {
	private static BufferedReader br = new BufferedReader(
			new InputStreamReader(System.in));
	private static Bank pankki;
	
	public static void main(String[] args) {
		String tilinumero = null;
		String luottoraja = null;
		int valinta;
		int rahamäärä = 0;
		pankki = new Pankki();
		while (true) {
			valinta = valikko(0, 7);
			if (valinta == 1) {
				System.out.print("Syötä tilinumero: ");
				tilinumero = inputString(tilinumero);
				System.out.print("Syötä rahamäärä: ");
				rahamäärä = inputInt(rahamäärä);
				pankki.lisääTili(tilinumero, rahamäärä);
			} else if (valinta == 2) {
				System.out.print("Syötä tilinumero: ");
				tilinumero = inputString(tilinumero);
				System.out.print("Syötä rahamäärä: ");
				rahamäärä = inputInt(rahamäärä);
				System.out.println("Syötä luottoraja: ");
				luottoraja = inputString(luottoraja);
				pankki.lisääLuottoTili(tilinumero, rahamäärä, luottoraja);
			} else if (valinta == 3) {
				System.out.print("Syötä tilinumero: ");
				tilinumero = inputString(tilinumero);
				System.out.print("Syötä rahamäärä: ");
				rahamäärä = inputInt(rahamäärä);
				pankki.talletaRahaa(tilinumero, rahamäärä);
			} else if (valinta == 4) {
				System.out.print("Syötä tilinumero: ");
				tilinumero = inputString(tilinumero);
				System.out.print("Syötä rahamäärä: ");
				rahamäärä = inputInt(rahamäärä);
				pankki.nostaRahaa(tilinumero, rahamäärä);
			} else if (valinta == 5) {
				System.out.print("Syötä poistettava tilinumero: ");
				tilinumero = inputString(tilinumero);
				pankki.poistaTili(tilinumero);
			} else if (valinta == 6) {
				System.out.print("Syötä tulostettava tilinumero: ");
				tilinumero = inputString(tilinumero);
				pankki.tulostaTili(tilinumero);
			} else if (valinta == 7) {
				pankki.tulostaKaikki();
			} else if (valinta == 0) {
				break;
			}
		}
	}
	
	public static int valikko(int ala, int ylä) {
		int valinta;
		
		while (true) {
			System.out.print("\n*** PANKKIJÄRJESTELMÄ ***\n1) Lisää tavallinen tili\n2)" +
					" Lisää luotollinen tili\n3) Tallenna tilille rahaa\n4) Nosta tililtä\n" +
					"5) Poista tili\n6) Tulosta tili\n7) Tulosta kaikki tilit\n0) Lopeta\n");
			System.out.print("Valintasi: ");
			try {
				try {
					valinta = Integer.parseInt(br.readLine());
					if (valinta < ala || valinta > ylä) {
						System.out.println("Valintasi ei kelpaa.");
					}
					else {
						return valinta;
					}
				} catch (NumberFormatException ex) {
					System.out.println("Valinta ei kelpaa.");
				}
			} catch (IOException ex) {
			}
		}
	}
	
	public static String inputString(String muuttuja) {
		try {
			muuttuja = br.readLine();
		} catch (IOException ex) {
		}
		return muuttuja;
	}
	
	public static int inputInt(int muuttuja) {
		while (true) {
			try {
				try {
					muuttuja = Integer.parseInt(br.readLine());
					return muuttuja;
				} catch (NumberFormatException ex) {
					System.out.println("Anna luku!");
					System.out.println("Syötä rahamäärä: ");
				}
			} catch (IOException ex) {
			}
		}
	}
}

