package viikko6_4;

public abstract class Account {
	protected String tilinumero;
	protected int rahamäärä;
	protected String luottoraja;
	
	public int talletaRahaa(int y) {
		rahamäärä = rahamäärä + y;
		return rahamäärä;
	}
	
	public int nostaRahaa(int y) {
		rahamäärä = rahamäärä - y;
		return rahamäärä;
	}
	
	public void tulostaTili() {
		System.out.print("Tilinumero: " + tilinumero + " Tilillä rahaa: " + rahamäärä + "\n");
	}
	
}

class Tili extends Account {
	public Tili(String x, int y) {
		tilinumero = x;
		rahamäärä = y;
		System.out.println("Tili luotu.");
	}
}

class LuottoTili extends Account {
	public LuottoTili() {
		
	}
}
