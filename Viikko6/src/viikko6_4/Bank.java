package viikko6_4;

public abstract class Bank {
	private Account tili;
	
	public void lisääTili(String tilinumero, int rahamäärä) {
		tili = new Tili(tilinumero, rahamäärä);
	}
	
	public void lisääLuottoTili(String tilinumero, int rahamäärä, String luottoraja) {	
		System.out.println("Pankkiin lisätään: " + tilinumero + "," + rahamäärä + "," + luottoraja);
	}
	
	public void talletaRahaa(String tilinumero, int rahamäärä) {
		tili.talletaRahaa(rahamäärä);
	}
	
	public void nostaRahaa(String tilinumero, int rahamäärä) {
		tili.nostaRahaa(rahamäärä);
	}
	
	public void poistaTili(String tilinumero) {
		System.out.println("Tili poistettu.");
	}
	
	public void tulostaTili(String tilinumero) {
		tili.tulostaTili();
	}
	
	public void tulostaKaikki() {
		System.out.println("Kaikki tilit:");
	}
}

class Pankki extends Bank {
	public Pankki() {
	}
}
