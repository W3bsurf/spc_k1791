package viikko6_5;

import java.util.ArrayList;


public class Bank {
	private ArrayList<Account> tilit;
	
	public Bank() {
		tilit = new ArrayList<Account>();
	}
	
	public void lisääTili(String tilinumero, int rahamäärä) {
		Account tili = new Tili(tilinumero, rahamäärä);
		tilit.add(tili);
	}
	
	public void lisääLuottoTili(String tilinumero, int rahamäärä, int luottoraja) {	
		Account tili = new LuottoTili(tilinumero, rahamäärä, luottoraja);
		tilit.add(tili);
	}
	
	public void talletaRahaa(String tilinumero, int rahamäärä) {
		for (Account tili : tilit) {
			if (tili.getTiliNumero().equals(tilinumero) == true) {
				tili.talletaRahaa(rahamäärä);
				return;
			}
		}
		System.out.println("Tiliä ei löytynyt!");
	}
	
	public void nostaRahaa(String tilinumero, int rahamäärä) {
		for (Account tili : tilit) {
			if (tili.getTiliNumero().equals(tilinumero) == true) {
				tili.nostaRahaa(rahamäärä);
				return;
			}
		}
		System.out.println("Tiliä ei löytynyt!");
	}
	
	public void poistaTili(String tilinumero) {
		for (Account tili : tilit) {
			if (tili.getTiliNumero().equals(tilinumero) == true) {
				tilit.remove(tili);
				System.out.println("Tili poistettu.");
				return;
			}
		}
		System.out.println("Tiliä ei löytynyt!");
	}
	
	public void tulostaTili(String tilinumero) {
		for (Account tili : tilit) {
			if (tili.getTiliNumero().equals(tilinumero) == true) {
				tili.tulostaTili();
				return;
			}
		}
		System.out.println("Tiliä ei löytynyt!");
	}
	
	public void tulostaKaikki() {
		System.out.println("Kaikki tilit:");
		for (Account tili : tilit) {
			tili.tulostaTili();
		}
	}
}
