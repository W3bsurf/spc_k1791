package viikko6_5;

public abstract class Account {
	protected String tilinumero;
	protected int rahamäärä;
	
	public Account(String x, int y) {
		tilinumero = x;
		rahamäärä = y;
		System.out.println("Tili luotu.");
	}
	
	public void talletaRahaa(int y) {
		rahamäärä = rahamäärä + y;
	}
	
	public void nostaRahaa(int y) {
		if ((rahamäärä >= y) == true) {
			rahamäärä = rahamäärä - y;
		} else {
			System.out.println("Tilillä ei ole tarpeeksi rahaa!");
		}
	}
	
	public void tulostaTili() {
		System.out.print("Tilinumero: " + tilinumero + " Tilillä rahaa: " + rahamäärä + "\n");
	}
	
	public String getTiliNumero() {
		return tilinumero;
	}
}

class Tili extends Account {
	public Tili(String x, int y) {
		super(x, y); 
	}
}

class LuottoTili extends Account {
	private int luottoraja;
	
	public LuottoTili(String x, int y, int z) {
		super(x, y);
		luottoraja = z;
	}
	
	@Override
	public void tulostaTili() {
		System.out.print("Tilinumero: " + tilinumero + " Tilillä rahaa: " + rahamäärä + " Luottoraja: " + luottoraja + "\n");
	}
	
	@Override
	public void nostaRahaa(int y) {
		if (((rahamäärä + luottoraja) >= y) == true) {
			rahamäärä = rahamäärä - y;
		} else {
			System.out.println("Tilillä ei ole tarpeeksi rahaa!");
		}
	}
}
