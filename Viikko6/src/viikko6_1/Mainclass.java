package viikko6_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Mainclass {
	public static void main(String[] args) {
		int valinta;
		while (true) {
			valinta = valikko(0, 7);
			if (valinta == 1) {
				tili();
			} else if (valinta == 2) {
				luottoTili();
			} else if (valinta == 3) {
				tallennaTilille();
			} else if (valinta == 4) {
				nostaTililtä();
			} else if (valinta == 5) {
				poistaTili();
			} else if (valinta == 6) {
				tulostaTili();
			} else if (valinta == 7) {
				tulostaKaikki();
			} else if (valinta == 0) {
				break;
			}
		}
	}
	
	public static int valikko(int ala, int ylä) {
		int valinta;
		
		while (true) {
			System.out.print("\n*** PANKKIJÄRJESTELMÄ ***\n1) Lisää tavallinen tili\n2)" +
					" Lisää luotollinen tili\n3) Tallenna tilille rahaa\n4) Nosta tililtä\n" +
					"5) Poista tili\n6) Tulosta tili\n7) Tulosta kaikki tilit\n0) Lopeta\n");
			System.out.print("Valintasi: ");
			BufferedReader br = new BufferedReader(
					new InputStreamReader(System.in));
			try {
				try {
					valinta = Integer.parseInt(br.readLine());
					if (valinta < ala || valinta > ylä) {
						System.out.println("Valintasi ei kelpaa.");
					}
					else {
						return valinta;
					}
				} catch (NumberFormatException ex) {
					System.out.println("Valinta ei kelpaa.");
				}
			} catch (IOException ex) {
			}
		}
	}
	
	public static void tili() {
		String tilinumero = null;
		String rahamäärä = null;
		System.out.print("Syötä tilinumero: ");
		BufferedReader br = new BufferedReader(
				new InputStreamReader(System.in));
		try {
			tilinumero = br.readLine();
		} catch (IOException ex) {
		}
		System.out.print("Syötä rahamäärä: ");
		try {
			rahamäärä = br.readLine();
		} catch (IOException ex) {
		}
		System.out.println("Tilinumero: " + tilinumero);
		System.out.println("Rahamäärä: " + rahamäärä);
	}
	
	public static void luottoTili() {
		String tilinumero = null;
		String rahamäärä = null;
		String luottoraja = null;
		System.out.print("Syötä tilinumero: ");
		BufferedReader br = new BufferedReader(
				new InputStreamReader(System.in));
		try {
			tilinumero = br.readLine();
		} catch (IOException ex) {
		}
		System.out.print("Syötä rahamäärä: ");
		try {
			rahamäärä = br.readLine();
		} catch (IOException ex) {
		}
		System.out.print("Syötä luottoraja: ");
		try {
			luottoraja = br.readLine();
		} catch (IOException ex) {
		}
		System.out.println("Tilinumero: " + tilinumero);
		System.out.println("Rahamäärä: " + rahamäärä);
		System.out.println("Luotto: " + luottoraja);
	}
	
	public static void tallennaTilille() {
		String tilinumero = null;
		String rahamäärä = null;
		System.out.print("Syötä tilinumero: ");
		BufferedReader br = new BufferedReader(
				new InputStreamReader(System.in));
		try {
			tilinumero = br.readLine();
		} catch (IOException ex) {
		}
		System.out.print("Syötä rahamäärä: ");
		try {
			rahamäärä = br.readLine();
		} catch (IOException ex) {
		}
		System.out.println("Tilinumero: " + tilinumero);
		System.out.println("Rahamäärä: " + rahamäärä);
	}
	
	public static void nostaTililtä() {
		String tilinumero = null;
		String rahamäärä = null;
		System.out.print("Syötä tilinumero: ");
		BufferedReader br = new BufferedReader(
				new InputStreamReader(System.in));
		try {
			tilinumero = br.readLine();
		} catch (IOException ex) {
		}
		System.out.print("Syötä rahamäärä: ");
		try {
			rahamäärä = br.readLine();
		} catch (IOException ex) {
		}
		System.out.println("Tilinumero: " + tilinumero);
		System.out.println("Rahamäärä: " + rahamäärä);
	}
	
	public static void poistaTili() {
		String tilinumero = null;
		System.out.print("Syötä poistettava tilinumero: ");
		BufferedReader br = new BufferedReader(
				new InputStreamReader(System.in));
		try {
			tilinumero = br.readLine();
		} catch (IOException ex) {
		}
		System.out.println("Tilinumero: " + tilinumero);
	}
	
	public static void tulostaTili() {
		String tilinumero;
		System.out.print("Syötä tulostettava tilinumero: ");
		BufferedReader br = new BufferedReader(
				new InputStreamReader(System.in));
		try {
			tilinumero = br.readLine();
			System.out.println("Tilinumero: " + tilinumero);
		} catch (IOException ex) {
		}
	}
	
	public static void tulostaKaikki() {
		System.out.println("Tulostaa kaiken");
	}
}

