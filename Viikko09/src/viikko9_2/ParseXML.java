package viikko9_2;

import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ParseXML {
	
	private Document document;
	private LinkedHashMap<String, String> map;
	
	public LinkedHashMap<String, String> getMap() {
		return map;
	}
	
	public ParseXML(String content) {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			
			document = dBuilder.parse(new InputSource(new StringReader(content)));
			
			document.getDocumentElement().normalize();
			
			map = new LinkedHashMap<String, String>();
			parseCurrentData();
		} catch (SAXException | IOException | ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void parseCurrentData() {
		NodeList nodes = document.getElementsByTagName("TheatreArea");
		
		for(int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			Element e = (Element) node;

			map.put(getID("ID", e, ""), getName("Name", e, ""));
		}
	}
	
	private String getName(String tag, Element e, String attr) {
		return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
	}
	
	private String getID(String tag, Element e, String attr) {
		return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
	}

}
