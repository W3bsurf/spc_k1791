package viikko9_2;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class FXMLDocumentController implements Initializable {
	private static TheaterList theaterList = TheaterList.getInstance();
    @FXML
    private Button listButton;
    
    @FXML
    private Button searchButton;
    
    @FXML
    private TextArea movieArea;
    
    @FXML
    private TextField dayField;
    
    @FXML
    private TextField startField;
    
    @FXML
    private TextField endField;
    
    @FXML
    private TextField nameField;
    
    @FXML
    private ComboBox<String> movieCombo;
    
    @FXML
    public void moviesForCombos() {
    	for (int i = 0; i < theaterList.tList.size(); i++) {
    		movieCombo.getItems().add(theaterList.tList.get(i).getName());
    	}
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	moviesForCombos();
    }
}
