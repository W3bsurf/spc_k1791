package viikko9_5;

import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ParseXML {
	
	private Document document;
	private LinkedHashMap<String, String> map;
	
	public LinkedHashMap<String, String> getMap() {
		return map;
	}
	
	public ParseXML(String content, String tag) {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			
			document = dBuilder.parse(new InputSource(new StringReader(content)));
			
			document.getDocumentElement().normalize();
			
			map = new LinkedHashMap<String, String>();
			if (tag.equals("TheatreArea") == true) {
				parseTheaterData(tag);
			} else if (tag.equals("Show") == true) {
				parseShowData(tag);
			}
			
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	private void parseTheaterData(String tag) {
		NodeList nodes = document.getElementsByTagName(tag);
		
		for(int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			Element e = (Element) node;

			map.put(getItem("ID", e), getItem("Name", e));
		}
	}
	
	private void parseShowData(String tag) {
		NodeList nodes = document.getElementsByTagName(tag);
		
		
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			Element e = (Element) node;
			
			String new_id = getItem("ID", e);
			String new_title = getItem("Title", e);
			String new_start = getItem("dttmShowStart", e);
			String new_end = getItem("dttmShowEnd", e);
			String new_runTime = getItem("LengthInMinutes", e);
			String new_auditorium = getItem("TheatreAuditorium", e);
			String new_theater = getItem("Theatre", e);
			ShowList.sList.add(new Show(new_id, new_title, new_start.substring(11, 16), new_end.substring(11, 16), new_runTime, new_auditorium, new_theater));
			
		}
	}
	
	private String getItem(String tag, Element e) {
		return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
	} 
}
