package viikko9_5;

public class Show {
	public String id;
	public String title;
	public String start;
	public String end;
	public String runTime;
	public String auditorium;
	public String theater;
	
	public Show(String new_id, String new_title, String new_start, String new_end, String new_runTime, String new_auditorium, String new_theater) {
		id = new_id;
		title = new_title;
		start = new_start;
		end = new_end;
		runTime = new_runTime;
		auditorium = new_auditorium;
		theater = new_theater;
	
	}
	
	public String getID() {
		return id;
	}
	
	public String getTheater() {
		return theater;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getStart() {
		return start;
	}
	
	public String getEnd() {
		return end;
	}
	
	public String getRunTime() {
		return runTime;
	}
	
	public String getAuditorium() {
		return auditorium;
	}
}
