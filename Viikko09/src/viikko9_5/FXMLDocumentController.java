package viikko9_5;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public class FXMLDocumentController implements Initializable {
	private static TheaterList theaterList = TheaterList.getInstance();
    @FXML
    private Button listButton;
    
    @FXML
    private Button searchButton;
    
    @FXML
    private ListView<String> movieView;
    
    @FXML
    private TextField dayField;
    
    @FXML
    private TextField startField;
    
    @FXML
    private TextField endField;
    
    @FXML
    private TextField nameField;
    
    @FXML
    private ComboBox<String> movieCombo;
    
    @FXML
    public void movieSearch(ActionEvent event) throws MalformedURLException, IOException {
    	String movie = nameField.getText();
    	movieView.getItems().clear();
    	String id = "";
    	String date = dayField.getText();
    	for (int i = 0; i < theaterList.tList.size(); i++) {
    			id = theaterList.tList.get(i).getID();
    			new ShowList(id, date);
    			for (int x = 0; x < ShowList.sList.size(); x++) {
    				if (ShowList.sList.get(x).getTitle().toLowerCase().contains(movie.toLowerCase())) {
    					movieView.getItems().add(ShowList.sList.get(x).getTitle() + " Teatteri: " + ShowList.sList.get(x).getTheater() + " Sali: " + ShowList.sList.get(x).getAuditorium() + " Aikaväli: " +
    							 ShowList.sList.get(x).getStart() + "-" +
    							 ShowList.sList.get(x).getEnd());
    				}
    			}
    		}
    }
    
    
    public void listMovies(ActionEvent event) throws MalformedURLException, IOException, NullPointerException {
    	if (movieCombo.getValue().equals("Valitse teatteri") == true) {
    		return;
    	} else if (movieCombo.getValue().isEmpty() == false) {
    		String id = "";
        	int start1 = 0;
        	int start2 = 0;
        	int end = 0;
        	String date = dayField.getText();
	    	
	    	movieView.getItems().clear();
	    	for (int i = 0; i < theaterList.tList.size(); i++) {
	    		if (movieCombo.getValue().equals(theaterList.tList.get(i).getName()) == true) {
	    			id = theaterList.tList.get(i).getID();
	    		}
	    	}
	    	new ShowList(id, date);
	    	if (startField.getText().isEmpty() == true && endField.getText().isEmpty() == true) {
		    	for (int i = 0; i < ShowList.sList.size(); i++) {
		    		showsForViews(i);
	    		}
	    	} else if (startField.getText().isEmpty() == false && endField.getText().isEmpty() == false){
	    		for (int i = 0; i < ShowList.sList.size(); i++) {
	    			try {
	    				start1 = Integer.parseInt(ShowList.sList.get(i).getStart().replace(":", ""));
	    				start2 = Integer.parseInt(startField.getText().replace(":", ""));
	    				end = Integer.parseInt(endField.getText().replace(":", ""));
	    			} catch (NumberFormatException ex) {
	    			}
	    			if (start1 >= start2 && start1 <= end) {
	    				showsForViews(i);
	    			}
	    		}
	    	}  else if (startField.getText().isEmpty() == true && endField.getText().isEmpty() == false){
	    		for (int i = 0; i < ShowList.sList.size(); i++) {
	    			try {
	    				start1 = Integer.parseInt(ShowList.sList.get(i).getStart().replace(":", ""));
	    				end = Integer.parseInt(endField.getText().replace(":", ""));
	    			} catch (NumberFormatException ex) {
	    			}
	    			if (start1 <= end) {
	    				showsForViews(i);
	    			}
	    		} 
	    	}  else if (startField.getText().isEmpty() == false && endField.getText().isEmpty() == true){
	    		for (int i = 0; i < ShowList.sList.size(); i++) {
	    			try {
	    				start1 = Integer.parseInt(ShowList.sList.get(i).getStart().replace(":", ""));
	    				start2 = Integer.parseInt(startField.getText().replace(":", ""));
	    			} catch (NumberFormatException ex) {
	    			}
	    			if (start1 >= start2) {
	    				showsForViews(i);
	    			}
	    		}
	    	}
    	}
	}
    
    public void showsForViews(int i) {
    	movieView.getItems().add(ShowList.sList.get(i).getTitle() + " Aikaväli: " +
				 ShowList.sList.get(i).getStart() + "-" +
				 ShowList.sList.get(i).getEnd() + " Kesto: " +
				 ShowList.sList.get(i).getRunTime() + " min Sali: " +
				 ShowList.sList.get(i).getAuditorium());
    }
    
   
    public void moviesForCombos() {
    	for (int i = 0; i < theaterList.tList.size(); i++) {
    		movieCombo.getItems().add(theaterList.tList.get(i).getName());
    	}
    	movieCombo.setValue("Valitse teatteri");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	moviesForCombos();
    }
}
