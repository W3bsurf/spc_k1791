package viikko9_5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

public class TheaterList {
	public ArrayList<Theater> tList;
	
	private static TheaterList theaterList = null;

	
	public TheaterList() throws MalformedURLException, IOException {
		URL url = new URL("http://www.finnkino.fi/xml/TheatreAreas/");
		tList = new ArrayList <Theater> ();
		
    	BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
    	
    	String content = "";
    	String line;
    	
    	while((line = br.readLine()) != null) {
    		content += line + "\n";
    	}
    	
		String new_name;
    	String new_id;
    	ParseXML xml = new ParseXML(content, "TheatreArea");
    	
    	
    	for (Map.Entry<String, String> entry : xml.getMap().entrySet()) {
    		if(entry.getKey().equals("1029") == true ||entry.getKey().equals("1014") == true || entry.getKey().equals("1002") == true ||
    				entry.getKey().equals("1012") == true ||entry.getKey().equals("1021") == true || entry.getKey().equals("1041") == true) {
    			continue;
    		} else {
	    		new_id = entry.getKey();
	    		new_name = entry.getValue();
	    		tList.add(new Theater(new_name, new_id));
    		}
    	}
	}
	
	public static TheaterList getInstance() {
		if(theaterList == null) {
			try {
				theaterList = new TheaterList();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return theaterList;
	}
}
