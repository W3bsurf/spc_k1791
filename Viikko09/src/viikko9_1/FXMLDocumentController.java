package viikko9_1;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class FXMLDocumentController implements Initializable {
    @FXML
    private Button listButton;
    
    @FXML
    private Button searchButton;
    
    @FXML
    private TextArea movieArea;
    
    @FXML
    private TextField dayField;
    
    @FXML
    private TextField startField;
    
    @FXML
    private TextField endField;
    
    @FXML
    private TextField nameField;
    
    @FXML
    private ComboBox<String> movieCombo;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
}
