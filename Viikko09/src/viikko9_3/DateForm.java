package viikko9_3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateForm {
	public static String Date(String date) {
		SimpleDateFormat format = new SimpleDateFormat ("dd-MM-yyyy");
		Date pvm;
		String output = "";
			try {
				pvm = format.parse(date);
				output = pvm.toString();
				return output;
				
			}catch (ParseException e) {
				System.out.println("Anna päivämäärä!");
			}
			return output;
			
	}
}
