package viikko9_4;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public class FXMLDocumentController implements Initializable {
	private static TheaterList theaterList = TheaterList.getInstance();
    @FXML
    private Button listButton;
    
    @FXML
    private Button searchButton;
    
    @FXML
    private ListView<String> movieView;
    
    @FXML
    private TextField dayField;
    
    @FXML
    private TextField startField;
    
    @FXML
    private TextField endField;
    
    @FXML
    private TextField nameField;
    
    @FXML
    private ComboBox<String> movieCombo;
    
    @FXML
    public void listMovies(ActionEvent event) throws MalformedURLException, IOException {
    	String id = "";
    	int start1 = 0;
    	int start2 = 0;
    	int end = 0;
    	String date = dayField.getText();
    	
    	movieView.getItems().clear();
    	for (int i = 0; i < theaterList.tList.size(); i++) {
    		if (movieCombo.getValue().equals(theaterList.tList.get(i).getName()) == true) {
    			id = theaterList.tList.get(i).getID();
    		}
    	}
    	new ShowList(id, date);
    	if (startField.getText().isEmpty() == true && endField.getText().isEmpty() == true) {
	    	for (int i = 0; i < ShowList.sList.size(); i++) {
		    		movieView.getItems().add(ShowList.sList.get(i).getTitle() + " Aikaväli: " +
		    								 ShowList.sList.get(i).getStart() + "-" +
		    								 ShowList.sList.get(i).getEnd() + " Kesto: " +
		    								 ShowList.sList.get(i).getRunTime() + " min Sali: " +
		    								 ShowList.sList.get(i).getAuditorium());
    		}
    	} else if (startField.getText().isEmpty() == false && endField.getText().isEmpty() == false){
    		for (int i = 0; i < ShowList.sList.size(); i++) {
    			try {
    				start1 = Integer.parseInt(ShowList.sList.get(i).getStart().replace(":", ""));
    				start2 = Integer.parseInt(startField.getText().replace(":", ""));
    				end = Integer.parseInt(endField.getText().replace(":", ""));
    			} catch (NumberFormatException ex) {
    			}
    			if (start1 >= start2 && start1 <= end) {
    				movieView.getItems().add(ShowList.sList.get(i).getTitle() + " Aikaväli: " +
											 ShowList.sList.get(i).getStart() + "-" +
											 ShowList.sList.get(i).getEnd() + " Kesto: " +
											 ShowList.sList.get(i).getRunTime() + " min Sali: " +
											 ShowList.sList.get(i).getAuditorium());
    			}
    		}
    	}  else if (startField.getText().isEmpty() == true && endField.getText().isEmpty() == false){
    		for (int i = 0; i < ShowList.sList.size(); i++) {
    			try {
    				start1 = Integer.parseInt(ShowList.sList.get(i).getStart().replace(":", ""));
    				end = Integer.parseInt(endField.getText().replace(":", ""));
    			} catch (NumberFormatException ex) {
    			}
    			if (start1 <= end) {
    				movieView.getItems().add(ShowList.sList.get(i).getTitle() + " Aikaväli: " +
											 ShowList.sList.get(i).getStart() + "-" +
											 ShowList.sList.get(i).getEnd() + " Kesto: " +
											 ShowList.sList.get(i).getRunTime() + " min Sali: " +
											 ShowList.sList.get(i).getAuditorium());
    			}
    		} 
    	}  else if (startField.getText().isEmpty() == false && endField.getText().isEmpty() == true){
    		for (int i = 0; i < ShowList.sList.size(); i++) {
    			try {
    				start1 = Integer.parseInt(ShowList.sList.get(i).getStart().replace(":", ""));
    				start2 = Integer.parseInt(startField.getText().replace(":", ""));
    			} catch (NumberFormatException ex) {
    			}
    			if (start1 >= start2) {
    				movieView.getItems().add(ShowList.sList.get(i).getTitle() + " Aikaväli: " +
											 ShowList.sList.get(i).getStart() + "-" +
											 ShowList.sList.get(i).getEnd() + " Kesto: " +
											 ShowList.sList.get(i).getRunTime() + " min Sali: " +
											 ShowList.sList.get(i).getAuditorium());
    			}
    		}
    	}
    }
    
    @FXML
    public void moviesForCombos() {
    	for (int i = 0; i < theaterList.tList.size(); i++) {
    		movieCombo.getItems().add(theaterList.tList.get(i).getName());
    	}
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	moviesForCombos();
    }
}
