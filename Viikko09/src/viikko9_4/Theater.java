package viikko9_4;

public class Theater {
	public String name;
	public String id;
	
	public Theater(String new_name, String new_id) {
		name = new_name;
		id = new_id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getID() {
		return id;
	}
}
