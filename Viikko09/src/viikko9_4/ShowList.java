package viikko9_4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class ShowList {
	public static ArrayList<Show> sList;
	
	public ShowList(String id,String date) throws MalformedURLException, IOException{
		URL url = new URL("http://www.finnkino.fi/xml/Schedule/?area=" + id +"&dt=" + date);
		sList = new ArrayList <Show> ();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
		
		String content = "";
		String line;
		
		while((line = br.readLine()) != null) {
			content += line + "\n";
		}
		
		new ParseXML(content, "Show");

	}	
}
