package viikko7_5;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

 
public class FXMLDocumentController implements Initializable {
    IOHandler tiedosonHozonsuru = new IOHandler();
    @FXML
    private Button saveButton;
    
    @FXML
    private Button loadButton;
    
    @FXML
    private Label label;
    
    @FXML
    private TextArea editField;
    
    @FXML
    private TextField tNimiField;
    
    @FXML
    private Label ohjeLabel;
    
    @FXML
    private void clickAndLoad(ActionEvent event) {
    	String teksti = tiedosonHozonsuru.read(tNimiField.getText());
        editField.setText(teksti);
    }
    
    @FXML
    private void clickAndSave(ActionEvent event) {
        String teksti = editField.getText();
        tiedosonHozonsuru.write(tNimiField.getText(), teksti);
    }
    
    @FXML
    private void inputText(KeyEvent event) {
    	label.setText(editField.getText());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    
}

