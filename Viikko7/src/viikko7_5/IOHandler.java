package viikko7_5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class IOHandler {
	
	public void write(String tiedosto, String teksti) {
		try {
			BufferedWriter out;
			
			out = new BufferedWriter(new FileWriter(tiedosto)); 
			
			out.write(teksti);
			out.close();
			System.out.println("Muokattiin: " + tiedosto);
		} catch (IOException ex) {
			System.out.println("Tiedostoa ei löytynyt!");
		}
	}
	
	public String read(String tiedosto) {
		String temporayHozonsuru = "";
		try {
			BufferedReader in;
			
			in = new BufferedReader(new FileReader(tiedosto));
			String inputLine;
			
			while ((inputLine = in.readLine()) != null) {
				temporayHozonsuru = temporayHozonsuru + inputLine + "\n";
			}
			in.close();
			System.out.println("Ladattiin: " + tiedosto);
		} catch (IOException ex) {
			System.out.println("Tiedostoa ei löytynyt!");
		}
		return temporayHozonsuru;
	}

}
