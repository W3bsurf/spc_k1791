package viikko7_3;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

 
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Button button;
    
    @FXML
    private Label label;
    
    @FXML
    private TextField userInput;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("Syötit tekstin!");
        label.setText(userInput.getText());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    
}

