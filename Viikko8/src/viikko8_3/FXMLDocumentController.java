package viikko8_3;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

 
public class FXMLDocumentController implements Initializable {
	BottleDispenser pulloMaatti = BottleDispenser.getInstance();
	double moneyInput;
    @FXML
    private Button kolikkoKolo;
    
    @FXML
    private Button buyButton;

    @FXML
    private Button returnButton;
    
    @FXML
    private TextArea printField;
    
    @FXML
    private TextField bottleChoice;
    
    @FXML
    private Label choicePrompt;
    
    @FXML
    private Slider moneySlider;
    
    @FXML
    private void buyBottle(ActionEvent event) {
    	int valinta;
    	try {
    		valinta = Integer.parseInt(bottleChoice.getText());
    	} catch (NumberFormatException ex) {
    		printField.setText("Virheellinen valinta!\n");
    		pulloMaatti.listBottles(printField);
    		return;
    	}
    	pulloMaatti.buyBottle(valinta, printField);
    	pulloMaatti.listBottles(printField);
    }
    
    @FXML
    private void setMoney(MouseEvent event) {
    	moneyInput = moneySlider.getValue();
    }
    
    @FXML
    private void returnMoney(ActionEvent event) {
    	pulloMaatti.returnMoney(printField);
    	pulloMaatti.listBottles(printField);
    }
    
    @FXML
    private void addMoney(ActionEvent event) {
    	pulloMaatti.addMoney(printField, moneyInput);
    	pulloMaatti.listBottles(printField);
    	moneySlider.setValue(0);
    	moneyInput = 0;
    	
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	pulloMaatti.listBottles(printField);
    }    
    
}
