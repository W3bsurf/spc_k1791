package viikko8_3;

import java.text.DecimalFormat;
import java.util.ArrayList;

import javafx.scene.control.TextArea;

public class BottleDispenser {
		
	private double money;
	private ArrayList <Bottle> pLista;
	
	private String df;
	

	private static BottleDispenser pulloMaatti = null;
	
	private BottleDispenser() {
		money = 0;
		pLista = new ArrayList <Bottle> ();
		pLista.add(new Bottle("Pepsi", "Pepsi Max", 0.67, 0.5, 1.8));
		pLista.add(new Bottle("Pepsi", "Pepsi Max", 0.67, 1.5, 2.2));
		pLista.add(new Bottle("Coca-Cola", "Coca-Cola Zero", 0.67, 0.5, 2.0));
		pLista.add(new Bottle("Coca-Cola", "Coca-Cola Zero", 0.67, 1.5, 2.5));
		pLista.add(new Bottle("Fanta", "Fanta Zero", 0.67, 0.5, 1.95));
		pLista.add(new Bottle("Fanta", "Fanta Zero", 0.67, 0.5, 1.95));
	}
	
	public static BottleDispenser getInstance() {
		if (pulloMaatti == null) {
			pulloMaatti = new BottleDispenser();
		}
		return pulloMaatti;
	}

	public void addMoney(TextArea printField, double moneyInput) {
		money += moneyInput;
		df = new DecimalFormat("0.00").format(money);
		System.out.println("Klink! Lisää rahaa laitteeseen!");
		printField.setText("Klink! Lisää rahaa laitteeseen! Laitteessa on nyt " + df + "€\n");
	}
	
	public void buyBottle(int valinta, TextArea printField) {
		if (valinta < 1 || valinta > pLista.size() && pLista.isEmpty() != true) {
			printField.setText("Virheellinen valinta!\n");
			return;
		}
		if (pLista.isEmpty() == true) {
			System.out.println("Pullot on loppu!");
			printField.setText("Pullot on loppu!\n");
			return;
		} else {
			if (money < pLista.get(valinta - 1).getPrice()) {
				System.out.println("Syötä rahaa ensin!");
				printField.setText("Syötä rahaa ensin!\n");
			}
			else {
					System.out.println("KACHUNK! " + pLista.get(valinta - 1).getName() + " tipahti masiinasta!");
					printField.setText("KACHUNK! " + pLista.get(valinta - 1).getName() + " tipahti masiinasta!\n");
					money -= pLista.get(valinta - 1).getPrice();
					pLista.remove(valinta - 1);
				}
		}
	}
	
	public void returnMoney(TextArea printField) {
		System.out.print("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + df + "€\n");
		printField.setText("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + df + "€\n");
		money = 0;
	}
	
	public void listBottles(TextArea printField) {
		int y = 0;
		for (int i = 0; i < pLista.size(); i++) {
			y++;
			System.out.println(y + ". Nimi: " + pLista.get(i).getName() + "\n\tKoko: " + pLista.get(i).getSize() + "\tHinta: " + pLista.get(i).getPrice());
			printField.appendText(y + ". Nimi: " + pLista.get(i).getName() + "\tKoko: " + pLista.get(i).getSize() + " Hinta: " + pLista.get(i).getPrice() + "\n");
		}
	}	
}
