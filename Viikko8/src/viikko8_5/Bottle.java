package viikko8_5;

public class Bottle {
	public String nimi;
	public String neste;
	public double energia;
	public double koko;
	public double hinta;
	
	public Bottle(String n1, String n2, double e, double k, double h) {
		nimi = n1;
		neste = n2;
		energia = e;
		koko = k;
		hinta = h;
	}

	public String getName() {
		return neste;
	}
	
	public String getManufacturer() {
		return nimi;
	}
	
	public double getEnergy() {
		return energia;
	}
	
	public double getPrice() {
		return hinta;
	}
	
	public double getSize() {
		return koko;
	}

}
