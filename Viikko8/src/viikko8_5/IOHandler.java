package viikko8_5;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class IOHandler {
	
	public void printReceipt(String tiedosto, String kuitti) {
		try {
			BufferedWriter out;
			
			out = new BufferedWriter(new FileWriter(tiedosto)); 
			
			out.write("Kuitti viimeisimmästä ostoksesta:\n" + kuitti);
			out.close();
		} catch (IOException ex) {
			System.out.println("Tiedostoa ei löytynyt!");
		}
	}
}
