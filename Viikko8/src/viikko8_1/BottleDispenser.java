package viikko8_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class BottleDispenser {
		
	private double money;
	private ArrayList <Bottle> pLista;
	
	private String df;
	
	private static BottleDispenser pulloMaatti = null;
	
	private BottleDispenser() {
		money = 0;
	
		pLista = new ArrayList <Bottle> ();
		pLista.add(new Bottle("Pepsi", "Pepsi Max", 0.67, 0.5, 1.8));
		pLista.add(new Bottle("Pepsi", "Pepsi Max", 0.67, 1.5, 2.2));
		pLista.add(new Bottle("Coca-Cola", "Coca-Cola Zero", 0.67, 0.5, 2.0));
		pLista.add(new Bottle("Coca-Cola", "Coca-Cola Zero", 0.67, 1.5, 2.5));
		pLista.add(new Bottle("Fanta", "Fanta Zero", 0.67, 0.5, 1.95));
		pLista.add(new Bottle("Fanta", "Fanta Zero", 0.67, 0.5, 1.95));
	}
	
	public static BottleDispenser getInstance() {
		if (pulloMaatti == null) {
			pulloMaatti = new BottleDispenser();
		}
		return pulloMaatti;
	}

	public void addMoney() {
		money += 1;
		System.out.println("Klink! Lisää rahaa laitteeseen!");
	}
	
	public void buyBottle() {
		int y = 0;
		int valinta = 0;
		for (int i = 0; i < pLista.size(); i++) {
			y++;
			System.out.println(y + ". Nimi: " + pLista.get(i).getName() + "\n\tKoko: " + pLista.get(i).getSize() + "\tHinta: " + pLista.get(i).getPrice());
		}
		if (pLista.isEmpty() == true) {
			System.out.println("Pullot on loppu!");
			return;
		} else {
			System.out.print("Valintasi: ");
			BufferedReader br = new BufferedReader(
					new InputStreamReader(System.in));
			try {
				valinta = Integer.parseInt(br.readLine());
			} catch (IOException ex) {
			}
			if (money < pLista.get(valinta - 1).getPrice()) {
				System.out.println("Syötä rahaa ensin!");
			}
			else {
					System.out.println("KACHUNK! " + pLista.get(valinta - 1).getName() + " tipahti masiinasta!");
					money -= pLista.get(valinta - 1).getPrice();
					pLista.remove(valinta - 1);
				}
		}
	}
	
	public void returnMoney() {
		df = new DecimalFormat("0.00").format(money);
		System.out.print("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + df + "€\n");
		money = 0;
	}
	
	public void listBottles() {
		int x = 0;
		for (int i = 0; i < pLista.size(); i++) {
			x++;
			System.out.println(x + ". Nimi: " + pLista.get(i).getName() + "\n\tKoko: " + pLista.get(i).getSize() + "\tHinta: " + pLista.get(i).getPrice());
		}
	}
	
	public int valikko() {
		int valinta;
		while (true) {
			System.out.print("\n*** LIMSA-AUTOMAATTI ***\n1) Lisää rahaa koneeseen\n2) Osta pullo\n3) Ota rahat ulos\n4) Listaa koneessa olevat pullot\n0) Lopeta\n");
			System.out.print("Valintasi: ");
			BufferedReader br = new BufferedReader(
					new InputStreamReader(System.in));
			try {
				try {
					valinta = Integer.parseInt(br.readLine());
					if (valinta < 0 || valinta > 4) {
						System.out.println("Valitse 0-4");
					}
					else {
						return valinta;
					}
				} catch (NumberFormatException ex) {
					System.out.println("Valitse 0-4");
				}
			} catch (IOException ex) {
			}
		}
	}		
}
