package viikko8_2;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

 
public class FXMLDocumentController implements Initializable {
	BottleDispenser pulloMaatti = BottleDispenser.getInstance();
    @FXML
    private Button kolikkoKolo;
    
    @FXML
    private Button buyButton;

    @FXML
    private Button returnButton;
    
    @FXML
    private TextArea printField;
    
    @FXML
    private TextField bottleChoice;
    
    @FXML
    private Label choicePrompt;
    
    @FXML
    private void buyBottle(ActionEvent event) {
    	int valinta;
    	try {
    		valinta = Integer.parseInt(bottleChoice.getText());
    	} catch (NumberFormatException ex) {
    		printField.setText("Virheellinen valinta!\n");
    		pulloMaatti.listBottles(printField);
    		return;
    	}
    	pulloMaatti.buyBottle(valinta, printField);
    	pulloMaatti.listBottles(printField);
    }
    
    @FXML
    private void returnMoney(ActionEvent event) {
    	pulloMaatti.returnMoney(printField);
    	pulloMaatti.listBottles(printField);
    }
    
    @FXML
    private void addMoney(ActionEvent event) {
    	pulloMaatti.addMoney(printField);
    	pulloMaatti.listBottles(printField);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	pulloMaatti.listBottles(printField);
    }    
    
}
