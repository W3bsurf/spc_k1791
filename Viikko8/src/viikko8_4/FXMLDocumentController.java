package viikko8_4;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

 
public class FXMLDocumentController implements Initializable {
	BottleDispenser pulloMaatti = BottleDispenser.getInstance();
	double moneyInput;
    @FXML
    private Button kolikkoKolo;
    
    @FXML
    private Button buyButton;

    @FXML
    private Button returnButton;
    
    @FXML
    private TextArea printField;
    
    @FXML
    private TextField bottleChoice;
    
    @FXML
    private Label choicePrompt;
    
    @FXML
    private Slider moneySlider;
    
    @FXML
    private ComboBox<String> bottleCombo;
    
    @FXML
    private void buyBottle(ActionEvent event) {
    	if (pulloMaatti.pLista.size() > 0) {
	    	for (int i = 0; i < pulloMaatti.pLista.size(); i++) {
	    		if ((bottleCombo.getValue().equals(pulloMaatti.pLista.get(i).getName() + " " + pulloMaatti.pLista.get(i).getSize() + "l " + pulloMaatti.pLista.get(i).getPrice() + "€")) == true) {
	    			pulloMaatti.buyBottle(i, printField);
	    			return;
	    		} else {
	    			printField.setText("Pullo on loppunut!");
	    		}
	    	}
    	} else {
    		printField.setText("Pullot ovat loppuneet!");
    	}
    }
    
    @FXML
    private void returnMoney(ActionEvent event) {
    	pulloMaatti.returnMoney(printField);
    }
    
    @FXML
    private void addMoney(ActionEvent event) {
    	moneyInput = moneySlider.getValue();
    	pulloMaatti.addMoney(printField, moneyInput);
    	moneySlider.setValue(0);
    	moneyInput = 0;
    }
    
    @FXML
    public void bottlesForCombos() {
    	for (int i = 0; i < pulloMaatti.pLista.size(); i++) {
    		bottleCombo.getItems().add(pulloMaatti.pLista.get(i).getName() + " " + pulloMaatti.pLista.get(i).getSize() + "l " + pulloMaatti.pLista.get(i).getPrice() + "€");
    	}
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	printField.setText("Tervetuloa käyttämään Bottle O'Matic-laitetta!");
    	bottlesForCombos();
    }  
}
