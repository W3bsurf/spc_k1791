package viikko10_3;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;

public class FXMLWebViewController implements Initializable {
	@FXML
	private WebView webView;
	
	@FXML
	private TextField addressBar;
	
	@FXML
	private Button searchButton;
	
	@FXML
	private Button refreshButton;
	
	@FXML
	private Button resetButton;
	
	@FXML
	private Button scriptButton;
	
	@FXML
	public void scriptEvent(ActionEvent event) {
		webView.getEngine().executeScript("document.shoutOut()");
	}
	
	@FXML
	public void resetScript(ActionEvent event) {
		webView.getEngine().executeScript("initialize()");
	}
	
	@FXML
	public void openSite(ActionEvent event) {
		if (addressBar.getText().equals("index2.html") == true) {
			webView.getEngine().load(getClass().getResource("index2.html").toExternalForm());
		} else {
		webView.getEngine().load("http://" + addressBar.getText());
		}
	}
	
	@FXML
	public void refreshSite(ActionEvent event) {
		webView.getEngine().load(webView.getEngine().getLocation());
	}
	
	@Override
    public void initialize(URL url, ResourceBundle rb) {
        webView.getEngine().load("http://www.lut.fi");
    }

}
