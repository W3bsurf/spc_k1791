package viikko10_4;

import java.net.URL;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;

public class FXMLWebViewController implements Initializable {
	ArrayList<String> siteList = new ArrayList<String>();
	private String direction = "";
	private ListIterator<String> litr;
	@FXML
	private WebView webView;
	
	@FXML
	private TextField addressBar;
	
	@FXML
	private Button searchButton;
	
	@FXML
	private Button refreshButton;
	
	@FXML
	private Button resetButton;
	
	@FXML
	private Button scriptButton;
	
	@FXML
	private Button backButton;
	
	@FXML
	private Button fwdButton;
	
	@FXML
	public void siteFwd(ActionEvent event) {
		if (litr.hasNext()) {
			if (direction.equals("back") == true) {
				litr.next();
			}
			String site = litr.next();
			webView.getEngine().load(site);
			addressBar.setText(webView.getEngine().getLocation());
			direction = "forward";
		}
	}
	
	@FXML
	public void siteBack(ActionEvent event) {
		if (litr.hasPrevious()) {
			if (direction.equals("forward") == true) {
				litr.previous();
			}
			String site = litr.previous();
			webView.getEngine().load(site);
			addressBar.setText(webView.getEngine().getLocation());
			direction = "back";
		}
	}
	
	@FXML
	public void scriptEvent(ActionEvent event) {
		webView.getEngine().executeScript("document.shoutOut()");
	}
	
	@FXML
	public void resetScript(ActionEvent event) {
		webView.getEngine().executeScript("initialize()");
	}
	
	@FXML
	public void openSite(ActionEvent event) {
		if (addressBar.getText().equals("index2.html") == true) {
			webView.getEngine().load(getClass().getResource("index2.html").toExternalForm());
		} else {
			System.out.println("Lista: ");
			litr = siteList.listIterator();
			while (litr.hasNext()){
				System.out.println(litr.next());
				if (litr.hasNext() == false) {
					if (addressBar.getText().contains("http://")) {
						webView.getEngine().load(addressBar.getText());
					} else {
						webView.getEngine().load("http://" + addressBar.getText());
					}
					litr.add(webView.getEngine().getLocation());
					addressBar.setText(webView.getEngine().getLocation());
					direction = "forward";
				}
			}
		}
	}
	
	@FXML
	public void refreshSite(ActionEvent event) {
		webView.getEngine().load(webView.getEngine().getLocation());
	}
	
	@Override
    public void initialize(URL url, ResourceBundle rb) {
        webView.getEngine().load("http://www.lut.fi");
		siteList.add(webView.getEngine().getLocation());
		addressBar.setText(webView.getEngine().getLocation());
	}
}
