package viikko10_2;
	
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

public class Main extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        try {
			Stage webview = new Stage();
			
			Parent page = FXMLLoader.load(getClass().getResource("FXMLWebView.fxml"));
			
			Scene scene = new Scene(page);
			
			webview.setScene(scene);
			webview.show();
		} catch (IOException ex) {
			Logger.getLogger(FXMLWebViewController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
 
    public static void main(String[] args) {
        launch(args);
    }
}
