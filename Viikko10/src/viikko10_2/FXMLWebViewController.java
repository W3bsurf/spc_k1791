package viikko10_2;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;

public class FXMLWebViewController implements Initializable {
	@FXML
	private WebView webView;
	
	@FXML
	private TextField addressBar;
	
	@FXML
	private Button searchButton;
	
	@FXML
	private Button refreshButton;
	
	@FXML
	public void openSite(ActionEvent event) {
		webView.getEngine().load("http://" + addressBar.getText());
	}
	
	@FXML
	public void refreshSite(ActionEvent event) {
		webView.getEngine().load(webView.getEngine().getLocation());
	}
	
	@Override
    public void initialize(URL url, ResourceBundle rb) {
        webView.getEngine().load("http://www.lut.fi");
    }

}
