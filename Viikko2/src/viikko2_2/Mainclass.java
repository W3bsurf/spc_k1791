package viikko2_2;

public class Mainclass {
	public static void main(String[] args) {
		Dog koira1 = new Dog("Rekku");
		Dog koira2 = new Dog("Musti");
		
		koira1.Speak(koira1.getNimi(), "Hau!");
		koira2.Speak(koira2.getNimi(), "Vuh!");
	}	
}
