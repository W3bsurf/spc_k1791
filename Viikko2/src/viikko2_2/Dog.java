package viikko2_2;

public class Dog {
	private String nimi = "";
	
	public Dog(String koiran_nimi) {
		nimi = koiran_nimi;
		System.out.println("Hei, nimeni on " + koiran_nimi + "!");
	}
	public void Speak(String nimi, String haukku) {
		String haukahdus = haukku;
		System.out.println(nimi + ": " + haukahdus);
	}
	
	public String getNimi() {
		return nimi;
	}
	
}
