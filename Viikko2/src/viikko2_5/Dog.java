package viikko2_5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Dog {
	private String nimi = "";
	private String haukahdus = "";
	
	public Dog() {
		System.out.print("Anna koiralle nimi: ");
		BufferedReader br = new BufferedReader(
				new InputStreamReader(System.in));
		try {
			nimi = br.readLine();
		} catch (IOException ex) {
		}
		if (nimi.trim().isEmpty() == true) {
			nimi = "Doge";
		}
		System.out.print("Hei, nimeni on " + nimi + "\n");
	}
	
	public void Speak(String nimi) {
		String haukku = "";
		System.out.print("Mitä koira sanoo: ");
		BufferedReader br = new BufferedReader(
				new InputStreamReader(System.in));
		try {
			haukku = br.readLine();
		} catch (IOException ex) {
		}	
		haukahdus = haukku;
		String testattava = "";
		Scanner s = new Scanner(haukahdus);
		while (true) {
			try {
				testattava = s.next();
			} catch (NoSuchElementException e) {
				s.close();
				break;
			}
			try {
				int x = Integer.parseInt(testattava);
				System.out.println("Such integer: " + x);
				continue;
			} catch (NumberFormatException ex) {
				
			}
			if (testattava.contains("true") == true || testattava.contains("false") == true) {
				System.out.println("Such boolean: " + testattava);
				continue;
			}
			else {
				System.out.println(testattava);
			}
		}	
	}
	
	public String getNimi() {
		return nimi;
	}
}
