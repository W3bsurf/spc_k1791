package viikko2_4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Dog {
	private String nimi = "";
	private String haukahdus = "";
	
	public Dog() {
		System.out.print("Anna koiralle nimi: ");
		BufferedReader br = new BufferedReader(
				new InputStreamReader(System.in));
		try {
			nimi = br.readLine();
		} catch (IOException ex) {
		}
		if (nimi.trim().isEmpty() == true) {
			nimi = "Doge";
		}
		System.out.print("Hei, nimeni on " + nimi + "!\n");
	}
	
	public void Speak(String nimi) {
		String haukku = "";
		while (true) {
			System.out.print("Mitä koira sanoo: ");
			BufferedReader br = new BufferedReader(
					new InputStreamReader(System.in));
			try {
				haukku = br.readLine();
			} catch (IOException ex) {
			}
			if (haukku.trim().isEmpty() == true) {
				haukahdus = "Much wow!";
				System.out.print(nimi + ": " + haukahdus + "\n");	
			}
			else {
				haukahdus = haukku;
				System.out.print(nimi + ": " + haukahdus + "\n");
				break;
			}
		}
	
	}
	
	public String getNimi() {
		return nimi;
	}
}
