package viikko2_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Dog {
	private String nimi = "";
	private String haukahdus = "";
	
	public Dog() {
		System.out.println("Anna koiralle nimi:");
		BufferedReader br = new BufferedReader(
				new InputStreamReader(System.in));
		try {
			nimi = br.readLine();
		} catch (IOException ex) {
		}
		System.out.println("Hei, nimeni on " + nimi + "!");
	}
	
	public void Speak(String nimi) {
		System.out.println("Mitä koira sanoo:");
		BufferedReader br = new BufferedReader(
				new InputStreamReader(System.in));
		try {
			haukahdus = br.readLine();
		} catch (IOException ex) {
		}
		System.out.println(nimi + ": " + haukahdus);
	}
	
	public String getNimi() {
		return nimi;
	}
}
