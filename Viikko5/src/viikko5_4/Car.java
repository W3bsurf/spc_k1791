package viikko5_4;

public class Car {
	protected String name;
	
	public Car() {
		name = "Car";
		Body kori = new Body();
		Chassis alusta = new Chassis();
		Engine moottori = new Engine();
		Wheel rengas1 = new Wheel();
		Wheel rengas2 = new Wheel();
		Wheel rengas3 = new Wheel();
		Wheel rengas4 = new Wheel();
	}
	
	class Body {
		
		public Body() {
			Class<? extends Body> koriClass = this.getClass();
			name = koriClass.getSimpleName();
			System.out.println("Valmistetaan: " + name);
		}
		
	}
	
	class Chassis {
		
		public Chassis() {
			Class<? extends Chassis> alustaClass = this.getClass();
			name = alustaClass.getSimpleName();
			System.out.println("Valmistetaan: " + name);
		}
	}
	
	class Engine {
		
		public Engine() {
			Class<? extends Engine> moottoriClass = this.getClass();
			name = moottoriClass.getSimpleName();
			System.out.println("Valmistetaan: " + name);
		}
	}
	
	class Wheel {
		
		public Wheel () {
			Class<? extends Wheel> rengasClass = this.getClass();
			name = rengasClass.getSimpleName();
			System.out.println("Valmistetaan: " + name);
		}
	}

	public void print() {
		System.out.print("Autoon kuuluu:\n\tBody\n\tChassis\n\tEngine\n\t4 Wheel\n");
	}
}


