package viikko5_5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Mainclass {
	public static void main(String[] args) {
		int valinta1, valinta2, valinta3;
		Character hahmo = null;
		WeaponBehaviour ase = null;
		while (true) {
			valinta1 = valikko1();
			if (valinta1 == 1) {
				valinta2 = valikko2();
				if (valinta2 == 1) {
					hahmo = new King();
				} else if (valinta2 == 2) {
					hahmo = new Knight();
				} else if (valinta2 == 3) {
					hahmo = new Queen();
				} else if (valinta2 == 4) {
					hahmo = new Troll();
				}
				valinta3 = valikko3();
				if (valinta3 == 1) {
					ase = new KnifeBehaviour();
				} else if (valinta3 == 2) {
					ase = new AxeBehaviour();
				} else if (valinta3 == 3) {
					ase = new SwordBehaviour();
				} else if (valinta3 == 4) {
					ase = new ClubBehaviour();
				}
			} else if (valinta1 == 2) {
				hahmo.fight(ase.useWeapon());
			} else if (valinta1 == 0) {
				break;
			}
		}	
	}
	
	public static int valikko1() {
		int valinta1;
		while (true) {
			System.out.print("*** TAISTELUSIMULAATTORI ***\n1) Luo hahmo\n2) Taistele hahmolla\n0) Lopeta\n");
			System.out.print("Valintasi: ");
			BufferedReader br = new BufferedReader(
					new InputStreamReader(System.in));
			try {
				try {
					valinta1 = Integer.parseInt(br.readLine());
					if (valinta1 < 0 || valinta1 > 2) {
						System.out.println("Valitse 0-2");
					}
					else {
						return valinta1;
					}
				} catch (NumberFormatException ex) {
					System.out.println("Valitse 0-2");
				}
			} catch (IOException ex) {
			}
		}
	}
	
	public static int valikko2() {
		int valinta2;
		while (true) {
			System.out.print("Valitse hahmosi: \n1) Kuningas\n2) Ritari\n3) Kuningatar\n4) Peikko\n");
			System.out.print("Valintasi: ");
			BufferedReader br = new BufferedReader(
					new InputStreamReader(System.in));
			try {
				try {
					valinta2 = Integer.parseInt(br.readLine());
					if (valinta2 < 1 || valinta2 > 4) {
						System.out.println("Valitse 1-4");
					}
					else {
						return valinta2;
					}
				} catch (NumberFormatException ex) {
					System.out.println("Valitse 1-4");
				}
			} catch (IOException ex) {
			}
		}
	}
	
	public static int valikko3() {
		int valinta3;
		while (true) {
			System.out.print("Valitse aseesi: \n1) Veitsi\n2) Kirves\n3) Miekka\n4) Nuija\n");
			System.out.print("Valintasi: ");
			BufferedReader br = new BufferedReader(
					new InputStreamReader(System.in));
			try {
				try {
					valinta3 = Integer.parseInt(br.readLine());
					if (valinta3 < 1 || valinta3 > 4) {
						System.out.println("Valitse 1-4");
					}
					else {
						return valinta3;
					}
				} catch (NumberFormatException ex) {
					System.out.println("Valitse 1-4");
				}
			} catch (IOException ex) {
			}
		}
	}
}