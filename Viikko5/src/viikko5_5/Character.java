package viikko5_5;

public abstract class Character {
	protected String name;
	
	public Character() {
		name = "";
	}
	
	public void fight(String weapon) {
		System.out.println(name + " tappelee aseella " + weapon);
	}

	public String getName() {
		return name;
	}
	
}
class King extends Character {
	public King() {
		name = this.getClass().getSimpleName();
	}
}

class Knight extends Character {
	public Knight() {
		name = this.getClass().getSimpleName();
	}
}

class Queen extends Character {
	public Queen() {
		name = this.getClass().getSimpleName();
	}
}

class Troll extends Character {
	public Troll() {
		name = this.getClass().getSimpleName();
	}
}
	
abstract class WeaponBehaviour {
	protected String weapon;
	
	public WeaponBehaviour() {
		weapon = "";
	}
	
	public String useWeapon() {
		return weapon;
	}
}
	
class KnifeBehaviour extends WeaponBehaviour {
	public KnifeBehaviour() {
		weapon = "Knife";
	}
}

class AxeBehaviour extends WeaponBehaviour {
	public AxeBehaviour() {
		weapon = "Axe";
	}
}

class SwordBehaviour extends WeaponBehaviour {
	public SwordBehaviour() {
		weapon = "Sword";
	}
}

class ClubBehaviour extends WeaponBehaviour {
	public ClubBehaviour() {
		weapon = "Club";
	}
}		