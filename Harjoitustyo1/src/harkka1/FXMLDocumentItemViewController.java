package harkka1;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class FXMLDocumentItemViewController implements Initializable{
    @FXML
    private Label itemCreationViewLabel;

    @FXML
    private Label deleteItemLabel;
    
    @FXML
    private Label itemSizeLabel;

    @FXML
    private Label itemWeightLabel;

    @FXML
    private Label itemNameLabel;
    
    @FXML
    private Label itemDeleteLabel;
    
    @FXML
    private Label itemCreationLabel;

    @FXML
    private ComboBox<Item> deleteItemCombo;

    @FXML
    private Button cancelButton;

    @FXML
    private Button createItemButton;

    @FXML
    private RadioButton breakableRadio;

    @FXML
    private TextField itemSizeField;

    @FXML
    private TextField itemWeightField;

    @FXML
    private TextField itemNameField;
    
    @FXML
    private Button deleteItemButton;
    
    // Luo esineen tietokantaan ja listaan
    public void createItem(ActionEvent event) {
    	int breakable = breakableRadio.isSelected()? 1 : 0;
		String item = itemNameField.getText();
		String weightText = itemWeightField.getText();
		String sizeText = itemSizeField.getText();
		if (!item.isEmpty() && !weightText.isEmpty() && !sizeText.isEmpty()) {
			try {
				double weight = Double.parseDouble(weightText);
				double size = Double.parseDouble(sizeText);
				SqlManager.getPostInsert().insertItem(item, size, breakable, weight);
				SqlManager.getPostSelect().selectItem(ListManager.getItemList());
				itemCreationLabel.setText(item + " lisätty esinevarastoon.");
			} catch (NumberFormatException e1) {
				Popups.popup("Huomio", "Esineen luominen ei onnistunut!", "Anna esineen paino ja koko desimaaleina");
			}
		} else {
			Popups.popup("Huomio", "Esineen luominen ei onnistunut!", "Kaikkia vaadittuja arvoja ei ole syötetty");
		}
	}
    
    // Populoi itemComboBoxin, josta voi valita poistettavan esineen
    public void itemsForCombos() {
		deleteItemCombo.getItems().clear();
		for (Item item: ListManager.getItemList()) {
			deleteItemCombo.getItems().add(item);
		}
	}
    
    // Poistaa comboboxissa valittuna olevan esineen tietokannasta ja listasta
    public void deleteItem(ActionEvent event) {
		if (deleteItemCombo.getValue() != null) {
			itemDeleteLabel.setText(deleteItemCombo.getValue().getName() + " poistettu esinevarastosta.");
			SqlManager.getPostDelete().deleteItem(deleteItemCombo.getValue().getID());
			SqlManager.getPostSelect().selectItem(ListManager.getItemList());
			deleteItemCombo.getSelectionModel().clearSelection();
		} else {
			Popups.popup("Huomio", "Esineen poistaminen ei onnistunut!", "Valitse ensin poistettava esine");
		}
	}
    
    // Sulkee nykyisen ikkunan
    public void cancel(ActionEvent event) {
		Stage cStage = (Stage)((Node)event.getSource()).getScene().getWindow();
		cStage.close();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
   	}
}
