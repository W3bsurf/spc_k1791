package harkka1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class Insert {
	
	// Syöttää toimitusluokat tietokantaan
	public void insertShipmentClass() {
		String sql1 = "INSERT INTO Toimitusluokka(Luokka, Kokorajoitus, Hajottava, Matkarajoitus) VALUES(?,?,?,?)";
		String sql2 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return;
			}
			PreparedStatement pstmt = conn.prepareStatement(sql1);
			
			pstmt.setInt(1, 1);
			pstmt.setDouble(2, 100);
			pstmt.setInt(3, 1);
			pstmt.setInt(4, 150);
			pstmt.addBatch();
			
			pstmt.setInt(1, 2);
			pstmt.setDouble(2, 40);
			pstmt.setInt(3, 0);
			pstmt.setInt(4, 10000);
			pstmt.addBatch();
			
			pstmt.setInt(1, 3);
			pstmt.setDouble(2, 600);
			pstmt.setInt(3, 1);
			pstmt.setInt(4, 10000);
			pstmt.addBatch();
			
			pstmt.executeBatch();
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setString(1, "Tietokantaan lisättiin toimitusluokat");
			pstmt2.executeUpdate();
			
			conn.commit();
		} catch(SQLException e) {
			System.out.println(e.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua.");
			}
		}
	}
	
	// Syöttää pakettiin lisätyn esineen tietokantaan ja palauttaa luodun esineen id:n
	public int insertItemSent(String item, double size, int breakable, double weight) {
		String sql1 = "INSERT INTO Sisältö(Tyyppi, Koko, Särkyvä, Paino, Lähetetty) VALUES(?,?,?,?,?)";
		String sql2 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return 0;
			}
			PreparedStatement pstmt = conn.prepareStatement(sql1, Statement.RETURN_GENERATED_KEYS);
			
			pstmt.setString(1, item);
			pstmt.setDouble(2, size);
			pstmt.setInt(3, breakable);
			pstmt.setDouble(4, weight);
			pstmt.setDouble(5, 1);
			
			pstmt.executeUpdate();
			
			ResultSet rs = pstmt.getGeneratedKeys();
		    int id = rs.getInt(1);
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setString(1, "Tietokantaan lisättiin esine ID:" + id);
			pstmt2.executeUpdate();
			
			conn.commit();
			return id;
		} catch(SQLException e) {
			System.out.println(e.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua.");
			}
		}
		return 0;
	}
	
	// Syöttää esineen tietokantaan
	public void insertItem(String item, double size, int breakable, double weight) {
		String sql1 = "INSERT INTO Sisältö(Tyyppi, Koko, Särkyvä, Paino) VALUES(?,?,?,?)";
		String sql2 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return;
			}
			PreparedStatement pstmt = conn.prepareStatement(sql1);
			
			pstmt.setString(1, item);
			pstmt.setDouble(2, size);
			pstmt.setInt(3, breakable);
			pstmt.setDouble(4, weight);
			pstmt.executeUpdate();
			
			ResultSet rs = pstmt.getGeneratedKeys();
		    int id = rs.getInt(1);
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setString(1, "Tietokantaan lisättiin esine ID:" + id);
			pstmt2.executeUpdate();
			
			conn.commit();
		} catch(SQLException e) {
			System.out.println(e.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua.");
			}
		}
	}
	
	// Syöttää 4 esinettä tietokantaan tietokantaa luodessa
	public void insertStartItems() {
		String sql1 = "INSERT INTO Sisältö(Tyyppi, Koko, Särkyvä, Paino) VALUES(?,?,?,?)";
		String sql2 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return;
			}
			PreparedStatement pstmt1 = conn.prepareStatement(sql1);
			
			pstmt1.setString(1, "Kivi");
			pstmt1.setDouble(2, 300);
			pstmt1.setInt(3, 0);
			pstmt1.setDouble(4, 1000);
			pstmt1.addBatch();
			
			pstmt1.setString(1, "Puhelin");
			pstmt1.setDouble(2, 10);
			pstmt1.setInt(3, 1);
			pstmt1.setDouble(4, 20);
			pstmt1.addBatch();
			
			pstmt1.setString(1, "Televisio");
			pstmt1.setDouble(2, 8);
			pstmt1.setInt(3, 1);
			pstmt1.setDouble(4, 10);
			pstmt1.addBatch();
			
			pstmt1.setString(1, "Tietokone");
			pstmt1.setDouble(2, 15);
			pstmt1.setInt(3, 0);
			pstmt1.setDouble(4, 30);
			pstmt1.addBatch();
			pstmt1.executeBatch();
			
		    PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setString(1, "Tietokantaan lisättiin aloitusesineet ja paketit");
			pstmt2.executeUpdate();
		    
			conn.commit();
		} catch(SQLException e) {
			System.out.println(e.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua.");
			}
		}
	}
	
	// Syöttää paketin tietokantaan
	public void insertPackage(ArrayList<Package> packageList, int shippingClass, int itemID, double routeLength, String itemName, String startOffice, String endOffice) {
		String sql1 = "INSERT INTO Paketti(Toimitusluokka, SisältöID) VALUES(?,?)";
		String sql2 = "INSERT INTO Toimitus(Alkupiste, Päätepiste, PakettiID, Pituus) VALUES(?,?,?,?)";
		String sql3 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		String sql4 = "INSERT INTO Pakettiloki(Pakettitapahtuma) VALUES(?)";
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return;
			}
			PreparedStatement pstmt1 = conn.prepareStatement(sql1, Statement.RETURN_GENERATED_KEYS);
			
			pstmt1.setInt(1, shippingClass);
			pstmt1.setInt(2, itemID);
			pstmt1.executeUpdate();
			
			ResultSet rs = pstmt1.getGeneratedKeys();
		    int packageID = rs.getInt(1);
		    
		    packageList.add(new Package(packageID, shippingClass, itemID, routeLength, itemName, startOffice, endOffice));
		    
		    PreparedStatement pstmt2 = conn.prepareStatement(sql2);
		    
		    pstmt2.setString(1, startOffice);
		    pstmt2.setString(2, endOffice);
		    pstmt2.setInt(3, packageID);
		    pstmt2.setDouble(4, routeLength);
		    pstmt2.executeUpdate();
		    
		    PreparedStatement pstmt3 = conn.prepareStatement(sql3);
			
			pstmt3.setString(1, "Tietokantaan lisättiin pakettid ID:" + packageID);
			pstmt3.executeUpdate();
			
			PreparedStatement pstmt4 = conn.prepareStatement(sql4);
			    
		    pstmt4.setString(1, "Luotiin paketti nro:" + packageID + ".");
		    pstmt4.executeUpdate();
		    
			conn.commit();
		} catch(SQLException e) {
			System.out.println(e.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua.");
			}
		}
	}
	
	// Syöttää toimituksen tietokantaan
	public void insertDelivery(String startOffice, String endOffice, int packageID, double routeLength) {
		String sql = "INSERT INTO Toimitus(Alkupiste, Päätepiste, PakettiID, Pituus) VALUES(?,?,?,?)";
		String sql2 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return;
			}	
			PreparedStatement pstmt = conn.prepareStatement(sql,  Statement.RETURN_GENERATED_KEYS);
			
			pstmt.setString(1,  startOffice);
			pstmt.setString(2, endOffice);
			pstmt.setInt(3, packageID);
			pstmt.setDouble(4, routeLength);
			pstmt.executeUpdate();
			
			ResultSet rs = pstmt.getGeneratedKeys();
		    int id = rs.getInt(1);
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setString(1, "Tietokantaan lisättiin toimitus ID:" + id);
			pstmt2.executeUpdate();
			
			conn.commit();
		} catch (SQLException e1) {
			System.out.println(e1.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua.");
			}
		}
	}
	
	// Syöttää käyttäjän tietokantaan ja palauttaa tiedon käyttäjän luomisen onnistumisesta
	public int insertUser(String username, String password, String name) {
		String sql = "INSERT INTO Käyttäjä(Käyttäjätunnus, Salasana, Nimi) VALUES(?,?,?)";
		String sql2 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return 0;
			}
			PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			pstmt.setString(1, username);
			pstmt.setString(2, password);
			pstmt.setString(3, name);
			pstmt.executeUpdate();
			
			ResultSet rs = pstmt.getGeneratedKeys();
		    int id = rs.getInt(1);
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setString(1, "Tietokantaan lisättiin käyttäjä ID:" + id);
			pstmt2.executeUpdate();
			
			conn.commit();
			return 1;
		} catch (SQLException e1) {
			System.out.println(e1.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua.");
			}
		}
		return 0;
	}
	
	// Syöttää paketin saapumistapahtuman tietokantaan
	public void insertPackageArrived(int packageID) {
		String sql1 = "SELECT ToimitusID FROM Toimitus WHERE PakettiID = ?";
		String sql2 = "INSERT INTO Pakettiloki(Pakettitapahtuma) VALUES(?)";
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return;
			}
			PreparedStatement pstmt1 = conn.prepareStatement(sql1);
			
			pstmt1.setInt(1, packageID);
			
			ResultSet rs = pstmt1.executeQuery();
		    int deliveryID = rs.getInt(1);
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);

			pstmt2.setString(1, "Toimitus nro: " + deliveryID + " paketti nro: " + packageID + " saapui perille.");
			pstmt2.executeUpdate();
						
			conn.commit();
		} catch (SQLException e1) {
			System.out.println(e1.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua.");
			}
		}
	}
	
	// Syöttää esineen hajoamistapahtuman tietokantaan
	public void insertItemBrokeEvent(int packageID, int itemID) {
		String sql1 = "SELECT Sisältö.Tyyppi FROM Sisältö INNER JOIN Paketti ON Paketti.SisältöID = Sisältö.SisältöID"
				+ " WHERE PakettiID = ?";
		String sql2 = "SELECT ToimitusID FROM Toimitus WHERE PakettiID = ?";
		String sql3 = "INSERT INTO Pakettiloki(Pakettitapahtuma) VALUES(?)";
		String sql4 = "UPDATE Sisältö SET Ehjä = 0 WHERE SisältöID = ?";
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return;
			}
			PreparedStatement pstmt1 = conn.prepareStatement(sql1);
			
			pstmt1.setInt(1, packageID);
			
			ResultSet rs1 = pstmt1.executeQuery();
		    String itemName = rs1.getString("Tyyppi");
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setInt(1, packageID);
			
			ResultSet rs2 = pstmt2.executeQuery();
		    int deliveryID = rs2.getInt(1);
			
			PreparedStatement pstmt3 = conn.prepareStatement(sql3);

			pstmt3.setString(1, "Toimituksen nro: " + deliveryID + " paketin nro: " + packageID + " sisältö: " + itemName + " hajosi kuljetuksen aikana.");
			pstmt3.executeUpdate();
			
			PreparedStatement pstmt4 = conn.prepareStatement(sql4);
			
			pstmt4.setInt(1, itemID);
			pstmt4.executeUpdate();
						
			conn.commit();
		} catch (SQLException e1) {
			System.out.println(e1.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua.");
			}
		}
	}
}
