package harkka1;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class Delivery {
	int deliveryID;
	int shipmentClass;
	String startOffice;
	String endOffice;
	String itemName;
	
	
	// Delivery luokan rakentaja
	public Delivery(int new_deliveryID, int new_shipmentClass, String new_startOffice, String new_endOffice, String new_itemName) {
		deliveryID = new_deliveryID;
		shipmentClass = new_shipmentClass;
		startOffice = new_startOffice;
		endOffice = new_endOffice;
		itemName = new_itemName;
	}
	
	public int getDeliveryID() {
		return deliveryID;
	}
	
	public int getShipmentClass() {
		return shipmentClass;
	}
	
	public String getStartOffice() {
		return startOffice;
	}
	
	public String getEndOffice() {
		return endOffice;
	}
	
	public String getItemName() {
		return itemName;
	}
}
