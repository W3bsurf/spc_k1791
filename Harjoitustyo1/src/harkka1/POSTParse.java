package harkka1;

import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class POSTParse {
	
	private Document document;
	
	// Määrittelee documentin ja kutsuu xml parseria
	public POSTParse(String content) {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			
			document = dBuilder.parse(new InputSource(new StringReader(content)));
			document.getDocumentElement().normalize();
			
			parsePostData();		
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	// Parsii xml:ää ja sijoittaa sieltä saadut pakettiautomaatit tietokantaan
	private void parsePostData() {
		String sql1 = "INSERT INTO Koordinaatit(LNG, LAT) VALUES(?,?)";
		String sql2 = "INSERT INTO Sijainti(Katuosoite, Postinumero , Postitoimipaikka, LNG, LAT) VALUES(?,?,?,?,?)";
		String sql3 = "INSERT INTO Pakettiautomaatti(Toimipiste, Aukiolo, Katuosoite, Postinumero) VALUES(?,?,?,?)";
		String sql4 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		NodeList nodes = document.getElementsByTagName("place");
		ConnectDB connectDB = new ConnectDB();
		int status = connectDB.createDatabase("harkka.db");
		Connection conn = null;
		if (status == 1) {
			try {
				conn = connectDB.connect();
				if (conn == null) {
					return;
				}
				for (int i = 0; i < nodes.getLength(); i++) {
					Node node = nodes.item(i);
					Element e = (Element) node;
					
					String code = getItem("code", e);
					String city = getItem("city", e);
					String address = getItem("address", e);
					String availability = getItem("availability", e);
					String postoffice = getItem("postoffice", e);
					String lat = getItem("lat", e);
					String lng = getItem("lng", e);
					
					PreparedStatement pstmt1 = conn.prepareStatement(sql1);
					
					pstmt1.setString(1, lng);
					pstmt1.setString(2, lat);
					pstmt1.executeUpdate();
					
					PreparedStatement pstmt2 = conn.prepareStatement(sql2);
					
					pstmt2.setString(1, address);
					pstmt2.setString(2, code);
					pstmt2.setString(3, city.substring(0, 1).toUpperCase() + city.substring(1).toLowerCase());
					pstmt2.setString(4, lng);
					pstmt2.setString(5, lat);
					pstmt2.executeUpdate();
					
					PreparedStatement pstmt3 = conn.prepareStatement(sql3);
					
					pstmt3.setString(1, postoffice);
					pstmt3.setString(2, availability);
					pstmt3.setString(3, address);
					pstmt3.setString(4, code);
					pstmt3.executeUpdate();
					}
			PreparedStatement pstmt4 = conn.prepareStatement(sql4);
			
			pstmt4.setString(1, "Tietokantaan lisättiin pakettiautomaatit xml:stä");
			pstmt4.executeUpdate();	
			
			conn.commit();
			Insert postInsert = new Insert();
			postInsert.insertShipmentClass();
			postInsert.insertStartItems();
			} catch (SQLException e1) {
				System.out.println(e1.getLocalizedMessage());
			} finally {
				try {
					if (conn != null) {
						conn.close();
					}
				} catch (SQLException e2) {
					System.out.println("Yhteyttä ei voitu sulkea");
				}
			}
		}
	}
	
	// Palauttaa xml:stä määritellyt elementit
	private String getItem(String tag, Element e) {
		return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
	} 
}
