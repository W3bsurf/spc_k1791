package harkka1;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.event.EventHandler;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class FXMLDocumentLoginViewController implements Initializable{
	@FXML
	private Label loginTitleLabel;
	
	@FXML
	private Label usernameLabel;
	
	@FXML
	private Label passwordLabel;
	
	@FXML
	private Button loginButton;
	
	@FXML
	private Button newUserButton;
	
	@FXML
	private TextField usernameField;
	
	@FXML
	private PasswordField passwordField;
	
	// Tarkistaa löytyykö käyttäjän antama käyttäjä tietokannasta ja avaa kirjautumisen onnistuessa karttaikkunan
	public void loginUser(ActionEvent event) {
		String username = usernameField.getText();
		String password = passwordField.getText();
		if (!username.isEmpty() && !password.isEmpty()) {
			if (SqlManager.getPostSelect().selectUserCheck(username, password) == 1) {
				Stage stage = new Stage();
				Parent page;
				try {
					page = FXMLLoader.load(getClass().getResource("FXMLDocumentMapView.fxml"));
					Scene scene = new Scene(page);
					stage.setScene(scene);
					stage.setTitle("Pakettimaatti");
					stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
						@Override
						public void handle(WindowEvent event) {
							Platform.exit();
						}
					});
					scene.getStylesheets().add(Main.class.getResource("application.css").toExternalForm());
					stage.show();
					stage.setResizable(false);
					
					Stage cStage = (Stage)((Node)event.getSource()).getScene().getWindow();
					cStage.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				Popups.popup("Huomio", "Sisäänkirjautuminen ei onnistunut!", "Käyttäjätunnus tai salasana on väärä");
			}
		} else {
			Popups.popup("Huomio", "Sisäänkirjautuminen ei onnistunut!", "Kaikkia vaadittuja arvoja ei ole syötetty");
		}
	}
	
	// Avaa uuden käyttäjän luomisikkunan
	public void openNewUser(ActionEvent event) {
		Stage stage = new Stage();
		Parent page;
		try {
			page = FXMLLoader.load(getClass().getResource("FXMLDocumentNewUserView.fxml"));
			Scene scene = new Scene(page);
			stage.setScene(scene);
			stage.setTitle("Luo uusi käyttäjä");
			scene.getStylesheets().add(Main.class.getResource("application.css").toExternalForm());
			stage.show();
			stage.setResizable(false);
			
			Stage cStage = (Stage)((Node)event.getSource()).getScene().getWindow();
			cStage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
    public void initialize(URL url, ResourceBundle rb) {
		if (ListManager.getPostList().pList.size() < 1) {
			SqlManager.getPostSelect().selectPostOffice(ListManager.getPostList().pList);
		}
	}
}


