package harkka1;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class FXMLDocumentLogViewController implements Initializable{
	@FXML
	private ListView<String> logView;
	
	@FXML
	private ListView<String> eventLogView;
	
	@FXML
	private ListView<String> packageStorageView;
	
	@FXML
	private DatePicker packageLogDatePicker;
	
	@FXML
	private DatePicker eventLogDatePicker;
	
	// Lataa pakettiloki näkymää varten tekstin
	public void loadLogText() {
		String logText = SqlManager.getPostSelect().selectPackageLog();
		ArrayList<String> logList = new ArrayList<String>(Arrays.asList(logText.split("\n")));
		ObservableList<String> logList2 = FXCollections.observableArrayList(logList);
		logView.setItems(logList2);
	}
	
	// Lataa tapahtumaloki näkymää varten tekstin
	public void loadEventLogText() {
		String logText = SqlManager.getPostSelect().selectEventLog();
		ArrayList<String> logList = new ArrayList<String>(Arrays.asList(logText.split("\n")));
		ObservableList<String> logList2 = FXCollections.observableArrayList(logList);
		eventLogView.setItems(logList2);
	}
	
	// Lataa pakettivarasto näkymää varten pakettien tiedot
	public void loadPackageStorage() {
		String packageStorage = "";
		for(Package item: ListManager.getPackageList()) {
			packageStorage += "Paketti nro:" + item.getPackageID() + " Mistä: " + item.getStartOffice() + " Minne: " + item.getEndOffice() + " Matka: " + item.getRouteLength() + " km, Sisältö: " + item.getItemName() + ", Toimitusluokka: " + item.getShippingClass() + "\n";
		}
			
		for(Package item: ListManager.getPackageSentList()) {
			packageStorage += "Paketti nro:" + item.getPackageID() + " Mistä: " + item.getStartOffice() + " Minne: " + item.getEndOffice() + " Matka: " + item.getRouteLength() + "km, Sisältö: " + item.getItemName() + ", Toimitusluokka: " + item.getShippingClass() + ", Lähetetty\n";
		}
		ArrayList<String> storageList = new ArrayList<String>(Arrays.asList(packageStorage.split("\n")));
		ObservableList<String> storageList2 = FXCollections.observableArrayList(storageList);
		packageStorageView.setItems(storageList2);
	}
	
	// Suodattaa valitun päivämäärän perusteella pakettiloki tapahtumat
	public void filterLogText(ActionEvent event) {
		if (packageLogDatePicker.getValue() != null) {
			String date = packageLogDatePicker.getValue().toString();
			String logText = SqlManager.getPostSelect().selectPackageLog();
			
			ArrayList<String> logList = new ArrayList<String>(Arrays.asList(logText.split("\n")));
			ObservableList<String> logList2 = FXCollections.observableArrayList();
			for (int i = 0; logList.size() >  i; i++) {
				if (date != null) {
					if (logList.get(i).substring(0, 10).equals(date)) {
						logList2.add(logList.get(i));
					}
				} 
			}
			logView.setItems(logList2);
		}
	}
	
	// Suodattaa valitun päivämäärän perusteella tapahtumaloki tapahtumat
	public void filterEventLogText(ActionEvent event) {
		if (eventLogDatePicker.getValue() != null) {
			String date = eventLogDatePicker.getValue().toString();
			String logText = SqlManager.getPostSelect().selectEventLog();
			
			ArrayList<String> logList = new ArrayList<String>(Arrays.asList(logText.split("\n")));
			ObservableList<String> logList2 = FXCollections.observableArrayList();
			for (int i = 0; logList.size() >  i; i++) {
				if (date != null) {
					if (logList.get(i).substring(0, 10).equals(date)) {
						logList2.add(logList.get(i));
					}
				} 
			}
			eventLogView.setItems(logList2);
		}
	}
	
	@Override
    public void initialize(URL url, ResourceBundle rb) {
		loadLogText();
		loadEventLogText();
		loadPackageStorage();
	}
}
