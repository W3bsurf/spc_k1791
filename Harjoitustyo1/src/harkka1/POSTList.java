package harkka1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class POSTList {
	public ArrayList<PostOffice> pList = new ArrayList<PostOffice>();
	
	private static POSTList postList = null;
	
	
	// Lukee xml:ää ja antaa parserille kutsun yhteydessä xml:n sisällön
	public POSTList() throws MalformedURLException, IOException {
		URL url = new URL("http://smartpost.ee/fi_apt.xml");
		
    	BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
    	
    	String content = "";
    	String line;
    	
    	while((line = br.readLine()) != null) {
    		content += line + "\n";
    	}
    	new POSTParse(content);
	}
	
	public static POSTList getInstance() {
		if (postList == null) {
			try {
				postList = new POSTList();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return postList;
	}
}
