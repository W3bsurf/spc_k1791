package harkka1;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class Item {
	int itemID;
	String name;
	double size;
	int breakable;
	boolean intact;
	double weight;
	
	public Item(int new_itemID,String new_name, double new_size, int new_breakable, boolean new_intact, double new_weight) {
		itemID = new_itemID;
		name = new_name;
		size = new_size;
		breakable = new_breakable;
		intact = new_intact;
		weight = new_weight;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	public String getName() {
		return name;
	}
	
	public int getID() {
		return itemID;
	}
	
	public double getSize() {
		return size;
	}
	
	public int getBreakable() {
		return breakable;
	}
	
	public boolean getIntact() {
		return intact;
	}
	
	public double getWeight() {
		return weight;
	}
}
