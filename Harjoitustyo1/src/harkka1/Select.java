package harkka1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class Select {
	
	// Hakee tietokannasta pakettiautomaatit ja lisää ne annettuun listaan
	public void selectPostOffice(ArrayList<PostOffice> list) {
		String sql = "SELECT Toimipiste, Aukiolo, Pakettiautomaatti.Katuosoite, Pakettiautomaatti.Postinumero, Sijainti.Postitoimipaikka"
				+ " FROM Pakettiautomaatti INNER JOIN Sijainti ON Pakettiautomaatti.Katuosoite = Sijainti.Katuosoite AND Pakettiautomaatti.Postinumero = Sijainti.Postinumero";
		String sql2 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return;
			}
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {	
				list.add(new PostOffice(rs.getString("Toimipiste"), rs.getString("Aukiolo"), rs.getString("Katuosoite"), rs.getString("Postinumero"), rs.getString("Postitoimipaikka")));
			}
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setString(1, "Tietokannasta haettiin pakettiautomaatit");
			pstmt2.executeUpdate();
			conn.commit();
		} catch (SQLException e1) {
			System.out.println(e1.getMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua");
			}
		}
	}
	
	// Hakee tietokannasta annettujen pakettiautomaattien koordinaatit ja palauttaa listan koordinaateista
	public ArrayList<String> selectCoordinates(String startOffice, String endOffice) {
		ArrayList<String> coordinateList = new ArrayList<String>();
		String sql = "SELECT Sijainti.LAT, Sijainti.LNG"
				+ " FROM Sijainti INNER JOIN Pakettiautomaatti ON"
				+ " Pakettiautomaatti.Katuosoite = Sijainti.Katuosoite AND"
				+ " Pakettiautomaatti.Postinumero = Sijainti.Postinumero"
				+ " WHERE Toimipiste = ?";
		String sql1 = "SELECT Sijainti.LAT, Sijainti.LNG"
				+ " FROM Sijainti INNER JOIN Pakettiautomaatti ON"
				+ " Pakettiautomaatti.Katuosoite = Sijainti.Katuosoite AND"
				+ " Pakettiautomaatti.Postinumero = Sijainti.Postinumero"
				+ " WHERE Toimipiste = ?";
		String sql2 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		
		
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return null;
			}
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, startOffice);
			ResultSet rs = pstmt.executeQuery();
			
			PreparedStatement pstmt1 = conn.prepareStatement(sql1);
			pstmt1.setString(1, endOffice);
			ResultSet rs1 = pstmt1.executeQuery();
			
			coordinateList.add(rs.getString("LAT"));
			coordinateList.add(rs.getString("LNG"));
			coordinateList.add(rs1.getString("LAT"));
			coordinateList.add(rs1.getString("LNG"));
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setString(1, "Tietokannasta haettiin lähetyksen koordinaatit");
			pstmt2.executeUpdate();
			
			conn.commit();
			return coordinateList;
		} catch (SQLException e1) {
			System.out.println(e1.getMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua");
			}
		}
		return null;
		
	}
	
	// Hakee tietokannasta annetun pakettiautomaatin aukioloajat ja palauttaa aukiolotiedot
	public String selectOfficeInfo(String office) {
		String sql = "SELECT Aukiolo"
				+ "	FROM Pakettiautomaatti"
				+ " WHERE Toimipiste = ?";
		String sql2 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return null;
			}
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,  office);
			ResultSet rs = pstmt.executeQuery();
			
			String availability = rs.getString("Aukiolo");
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setString(1, "Tietokannasta haettiin pakettiautomaatin aukiolotiedot");
			pstmt2.executeUpdate();
			
			conn.commit();
			return availability;
		} catch (SQLException e1) {
			System.out.println(e1.getMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua");
			}
		}
		return null;
	}
	
	// Hakee tietokannasta annetun pakettiautomaatin osoitetiedot ja palauttaa ne
	public String selectOfficeMap(String office) {
		String sql = "SELECT Pakettiautomaatti.Katuosoite, Pakettiautomaatti.Postinumero, Sijainti.Postitoimipaikka"
				+ "	FROM Pakettiautomaatti INNER JOIN Sijainti ON Pakettiautomaatti.Katuosoite = Sijainti.Katuosoite"
				+ " AND Pakettiautomaatti.Postinumero = Sijainti.Postinumero"
				+ " WHERE Toimipiste = ?";
		String sql2 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return null;
			}
			PreparedStatement pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1,  office);
			
			ResultSet rs = pstmt.executeQuery();
			String info = rs.getString("Katuosoite") + ", " + rs.getString("Postinumero") + ", " + rs.getString("Postitoimipaikka");
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setString(1, "Tietokannasta haettiin pakettiautomaatin tiedot karttaa varten");
			pstmt2.executeUpdate();
			
			conn.commit();
			return info;
		} catch (SQLException e1) {
			System.out.println(e1.getMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua");
			}
		}
		return null;
	}
	
	// Valitsee esineet tietokannasta ja lisää ne listaan
	public void selectItem(ArrayList<Item> itemList) {
		String sql = "SELECT SisältöID, Tyyppi, Koko, Särkyvä, Ehjä, Paino"
				+ " FROM Sisältö"
				+ " WHERE Lähetetty = 0";
		String sql2 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		
		itemList.clear();
		
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return;
			}
			PreparedStatement pstmt = conn.prepareStatement(sql);
			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {		
				itemList.add(new Item(rs.getInt("SisältöID"), rs.getString("Tyyppi"), rs.getDouble("Koko"),rs.getInt("Särkyvä"), (rs.getInt("Ehjä") == 1), rs.getDouble("Paino")));
			}
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setString(1, "Tietokannasta haettiin lähettämättömät esineet");
			pstmt2.executeUpdate();
			
			conn.commit();
		} catch (SQLException e1) {
			System.out.println(e1.getMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua");
			}
		}
	}
	
	// Valitsee toimitusluokat tietokannnasta ja lisää ne listaan
	public void selectClass(ArrayList<ShipmentClass> shipmentClassList) {
		String sql = "SELECT Luokka, Kokorajoitus, Hajottava, Matkarajoitus"
				+ " FROM Toimitusluokka";
		String sql2 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
	
		shipmentClassList.clear();
		
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return;
			}
			PreparedStatement pstmt = conn.prepareStatement(sql);
			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				shipmentClassList.add(new ShipmentClass(rs.getInt("Luokka"), rs.getDouble("Kokorajoitus"), (rs.getInt("Hajottava") == 1), rs.getInt("Matkarajoitus")));
			}
				
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setString(1, "Tietokannasta haettiin toimitusluokat");
			pstmt2.executeUpdate();
			
			conn.commit();
		} catch (SQLException e1) {
			System.out.println(e1.getMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua");
			}
		}
	}
	
	/*
	 *  Valitsee paketit tietokannasta
	 *  Paketit lisätään joko lähetettyjen tai lähettämättömien pakettien listaan "Toimitettu" attribuutin perusteella
	 */
	public void selectPackage(ArrayList<Package> packageList, ArrayList<Package> packageSentList) {
		String sql1 = "SELECT Paketti.PakettiID, Paketti.Toimitusluokka, Paketti.SisältöID, Sisältö.Tyyppi, Toimitus.Alkupiste, Toimitus.Päätepiste, Toimitus.Pituus"
				+ " FROM Paketti INNER JOIN Sisältö ON Paketti.SisältöID = Sisältö.SisältöID"
				+ " INNER JOIN Toimitus ON Toimitus.PakettiID = Paketti.PakettiID"
				+ " WHERE Paketti.Toimitettu = 0"; 
		String sql2 = "SELECT Paketti.PakettiID, Paketti.Toimitusluokka, Paketti.SisältöID, Sisältö.Tyyppi, Toimitus.Alkupiste, Toimitus.Päätepiste, Toimitus.Pituus"
				+ " FROM Paketti INNER JOIN Sisältö ON Paketti.SisältöID = Sisältö.SisältöID"
				+ " INNER JOIN Toimitus ON Toimitus.PakettiID = Paketti.PakettiID"
				+ " WHERE Paketti.Toimitettu = 1"; 
		String sql3 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		
		packageList.clear();
		packageSentList.clear();
		
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return;
			}
			PreparedStatement pstmt1 = conn.prepareStatement(sql1);
	
			ResultSet rs1 = pstmt1.executeQuery();
			while (rs1.next()) {
					packageList.add(new Package(rs1.getInt("PakettiID"), rs1.getInt("Toimitusluokka"), rs1.getInt("SisältöID"), rs1.getDouble("Pituus"), rs1.getString("Tyyppi"), rs1.getString("Alkupiste"), rs1.getString("Päätepiste")));
			}
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			ResultSet rs2 = pstmt2.executeQuery();
			while (rs2.next()) {
					packageSentList.add(new Package(rs2.getInt("PakettiID"), rs2.getInt("Toimitusluokka"), rs2.getInt("SisältöID"), rs2.getDouble("Pituus"), rs2.getString("Tyyppi"), rs2.getString("Alkupiste"), rs2.getString("Päätepiste")));
			}
			
			PreparedStatement pstmt3 = conn.prepareStatement(sql3);
			
			pstmt3.setString(1, "Tietokannasta haettiin paketit");
			pstmt3.executeUpdate();
			
			conn.commit();
		} catch (SQLException e1) {
			System.out.println(e1.getMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua.");
			}
		}
	}
	
	// Tietokannasta haetaan toimitukset ja ne lisätään listaan
	public void selectDelivery(ArrayList<Delivery> deliveryList) {
		String sql = "SELECT Toimitus.ToimitusID, Toimitus.Alkupiste, Toimitus.Päätepiste, Sisältö.Tyyppi, Paketti.Toimitusluokka"
				+ " FROM Toimitus INNER JOIN Paketti ON Toimitus.PakettiID = Paketti.PakettiID"
				+ " INNER JOIN Sisältö ON Paketti.SisältöID = Sisältö.SisältöID"
				+ "	WHERE Toimitus.Toimitettu = 1";
		String sql2 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		
		deliveryList.clear();
		
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return;
			}
			PreparedStatement pstmt = conn.prepareStatement(sql);
			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				deliveryList.add(new Delivery(rs.getInt("ToimitusID"), rs.getInt("Toimitusluokka"), rs.getString("Alkupiste"), rs.getString("Päätepiste"), rs.getString("Tyyppi")));
			}
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setString(1, "Tietokannasta haettiin toimitukset");
			pstmt2.executeUpdate();
			
			conn.commit();
		} catch (SQLException e1) {
			System.out.println(e1.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua.");
			}
		}
	}
	
	/* 
	 * Tietokannasta haetaan annetun esineen ja toimitusluokan "Hajottava" ja "Särkyvä" attribuutit
	 * Attribuutteja verrataan keskenään ja tuloksen perusteella palautetaan esineen status
	 */
	
	public int selectItemStatusCheck(int itemID, int shippingClass) {
		String sql1 = "SELECT Hajottava FROM Toimitusluokka"
				+ " WHERE Luokka = ?";
		String sql2 = "SELECT Särkyvä FROM Sisältö"
				+ " WHERE SisältöID = ?";
		String sql3 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return 0;
			}
			PreparedStatement pstmt1 = conn.prepareStatement(sql1);
			
			pstmt1.setInt(1, shippingClass);
			
			ResultSet rs1 = pstmt1.executeQuery();
			int result1 = rs1.getInt("Hajottava");
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setInt(1, itemID);
			
			ResultSet rs2 = pstmt2.executeQuery();
			int result2 = rs2.getInt("Särkyvä");
			
			PreparedStatement pstmt3 = conn.prepareStatement(sql3);
			
			pstmt3.setString(1, "Tietokannasta tarkastettiin esineen särkyvyys");
			pstmt3.executeUpdate();
			
			conn.commit();
			if (result2 == 1 && result1 == 1) {
				return 1;
			} else {
				return 0;
			}
		} catch (SQLException e1) {
			System.out.println(e1.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua.");
			}
		}
		return 0;
	}
	
	// Tietokannasta haetaan pakettilokin tiedot ja ne palautetaan
	public String selectPackageLog() {
		String sql1 = "SELECT Pakettitapahtuma, Aikaleima FROM Pakettiloki";
		String result = "";
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return null;
			}
			PreparedStatement pstmt1 = conn.prepareStatement(sql1);

			ResultSet rs = pstmt1.executeQuery();
			while (rs.next()) {
				result += rs.getString("Aikaleima") + ": " + rs.getString("Pakettitapahtuma") + "\n";
			}
			
			conn.close();
			return result;
		} catch (SQLException e1) {
			System.out.println(e1.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua.");
			}
		}
		return result;
	}
	
	// Tietokannasta haetaan tapahtumalokin tiedot ja ne palautetaan
	public String selectEventLog() {
		String sql1 = "SELECT Tapahtuma, Aikaleima FROM Loki";
		String result = "";
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return null;
			}
			PreparedStatement pstmt1 = conn.prepareStatement(sql1);

			ResultSet rs = pstmt1.executeQuery();
			while (rs.next()) {
				result += rs.getString("Aikaleima") + ": " + rs.getString("Tapahtuma") + "\n";
			}
			
			conn.close();
			return result;
		} catch (SQLException e1) {
			System.out.println(e1.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua.");
			}
		}
		return result;
	}
	
	
	// Tietokannasta verrataan annettuja kirjautumistietoja ja palautetaan tieto kirjautumisen onnistumisesta
	public int selectUserCheck(String username, String password) {
		String sql = "SELECT COUNT(*) FROM Käyttäjä"
				+ " WHERE Käyttäjätunnus = ? AND Salasana = ?";
		String sql2 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return 0;
			}
			PreparedStatement pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, username);
			pstmt.setString(2, password);
			
			ResultSet rs = pstmt.executeQuery();
			int result = rs.getInt(1);
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setString(1, "Tietokannasta tarkastettiin käyttäjän kirjautuminen");
			pstmt2.executeUpdate();
			
			conn.commit();
			return result;
		} catch (SQLException e1) {
			System.out.println(e1.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua.");
			}
		}
		return 0;
	}
}
