package harkka1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.sqlite.SQLiteConfig;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class ConnectDB {
	
	// Luo tietokannan ja palauttaa tiedon siitä pitikö tietokanta luoda vai oliko se jo olemassa
	public int createDatabase(String new_fileName) {
		String url = "jdbc:sqlite:" + new_fileName;
		File file = new File(new_fileName);
		if (!file.exists()) {
			try {
				Connection conn = DriverManager.getConnection(url);
				if (conn != null) {
					conn.getMetaData();
					fillDataBase();
					return 1;
				}
			} catch (SQLException e1) {
				System.out.println(e1.getLocalizedMessage());
			}
		} else {
			return 0;
		}
		return 0;
	}

	// Palauttaa yhteyden tietokantaan
	public Connection connect() {
        Connection conn = null;
        try {
        	SQLiteConfig sqliteConfig = new SQLiteConfig();
        	sqliteConfig.enforceForeignKeys(true);
        	String url = "jdbc:sqlite:harkka.db";
            conn = DriverManager.getConnection(url, sqliteConfig.toProperties());
            conn.setAutoCommit(false);
        } catch (SQLException e1) {
            System.out.println(e1.getMessage());
        }
        return conn;
	}
	
	// Täyttää tietokannan sql tiedoston mukaisilla tauluilla
	private void fillDataBase() {
		ConnectDB connectDB = new ConnectDB();
		StringBuffer sb = new StringBuffer();
		String line;
		try {
			File file = new File("Tietokanta.sql");
			BufferedReader br = new BufferedReader(new FileReader(file));
			while((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();
			String content = sb.toString();
			Connection conn = connectDB.connect();
			Statement stmt = conn.createStatement();
			for (String item : content.split(";")) {
				stmt.addBatch(item + ";");
			}
			stmt.executeBatch();
			conn.commit();
		} catch(Exception e1) {
			e1.printStackTrace();
		}
	}
}
