package harkka1;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class FXMLDocumentNewUserViewController implements Initializable {
 
	@FXML
	private Label userCreationTitleLabel;
	
	@FXML
	private Label creationStatusLabel;
	
	@FXML
	private Label nameLabel;
	
	@FXML
	private Label usernameLabel;
	
	@FXML
	private Label passwordLabel;
	
	@FXML
	private TextField nameField;
	
	@FXML
	private TextField usernameField;
	
	@FXML
	private PasswordField passwordField;
	
	@FXML
	private Button stopCreationButton;
	
	@FXML
	private Button newUserButton;
	
	// Sulkee uuden käyttäjän luomisikkunan ja avaa sisäänkirjautumisikkunan
	public void stopCreation(ActionEvent event) {
		Stage stage = new Stage();
		Parent page;
		try {
			page = FXMLLoader.load(getClass().getResource("FXMLDocumentLoginView.fxml"));
			Scene scene = new Scene(page);
			stage.setScene(scene);
			stage.setTitle("Tervetuloa");
			scene.getStylesheets().add(Main.class.getResource("application.css").toExternalForm());
			stage.show();
			stage.setResizable(false);
			
			Stage cStage = (Stage)((Node)event.getSource()).getScene().getWindow();
			cStage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Luo annettujen tietojen perusteella käyttäjän tietokantaan
	public void createNewUser() {
		String name = nameField.getText();
		String username = usernameField.getText();
		String password = passwordField.getText();
		
		if (!name.isEmpty() && !username.isEmpty() && !password.isEmpty()) {
			int status = SqlManager.getPostInsert().insertUser(username, password, name);
			if (status == 0) {
				Popups.popup("Huomio", "Käyttäjän luominen ei onnistunut!", "Käyttäjätunnus on jo varattu");
			} else {
				creationStatusLabel.setText("Käyttäjän luominen onnistui!");
			}
		} else {
			Popups.popup("Huomio", "Käyttäjän luominen ei onnistunut!", "Kaikkia vaadittuja arvo ei ole syötetty");
		}
	}
	
	@Override
    public void initialize(URL url, ResourceBundle rb) {
	}
}
