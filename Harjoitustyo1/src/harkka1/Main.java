package harkka1;
	
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class Main extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        try {
			Parent page = FXMLLoader.load(getClass().getResource("FXMLDocumentLoginView.fxml"));
			
			Scene scene = new Scene(page);
			
			stage.setScene(scene);
			stage.setTitle("Tervetuloa");
			scene.getStylesheets().add(Main.class.getResource("application.css").toExternalForm());
			stage.show();
			stage.setResizable(false);
		} catch (IOException ex) {
			Logger.getLogger(FXMLDocumentLoginViewController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
 
    public static void main(String[] args) {
        launch(args);
    }
}
