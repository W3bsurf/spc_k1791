package viikko11_1;

import java.util.ArrayList;

import javafx.scene.shape.Line;

public class ShapeHandler {
	public ArrayList<Point> philList;
	private static ShapeHandler shapePhil = null;
	
	public ArrayList<Line> drPhilList;
	
	public ShapeHandler() {
		philList = new ArrayList<Point>();
		drPhilList = new ArrayList<Line>();
	}
	
	public void addPhil(Point point) {
		philList.add(point);
	}
	
	public void addDrPhil(Line line) {
		drPhilList.add(line);
	}
	
	public static ShapeHandler getInstance() {
		if (shapePhil == null) {
			shapePhil = new ShapeHandler();
		} 
		return shapePhil;
	}
}
