package viikko11_5;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class Main extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
    	Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        		
        stage.setHeight(1024);
        stage.setWidth(677);
        Scene scene = new Scene(root);
        stage.setResizable(false);
        stage.setScene(scene);
        root.setStyle("-fx-background-image: url('" + getClass().getResource("Suomen-kartta.jpg").toExternalForm() + "')");
        stage.setTitle("Den ständiga programmeringen");
        
        stage.show();
    }
 
    public static void main(String[] args) {
        launch(args);
    }
}
