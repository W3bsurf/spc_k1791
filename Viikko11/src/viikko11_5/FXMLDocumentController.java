package viikko11_5;


import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;



public class FXMLDocumentController implements Initializable {
	private static ShapeHandler shapePhil = ShapeHandler.getInstance();
	private Point point;
	protected double startX = 0; 
	protected double startY = 0;
	protected double endX = 0;
	protected double endY = 0;
	
	@FXML
	public AnchorPane anchorPane;
	
    public void createCircle(MouseEvent event) {
    	if (event.getTarget() instanceof AnchorPane) {
    		point = new Point();
			Circle circle =  point.createCircle(event.getSceneX(), event.getSceneY());
			
		    anchorPane.getChildren().add(circle);
		    shapePhil.addPhil(point);
		   
    	} else {
    		 for (Point point : shapePhil.philList) {
 	    		if (point.getChosen() == true) {
 	    			if (startX == 0 && startY == 0) {
 	    				startX = point.getX();
 	    				startY = point.getY();
 	    				point.setChosen();
 	    			} else if (startX != 0 && startY !=0 && startX != point.getX() && startY != point.getY()) {
 	    				endX = point.getX();
 	    				endY = point.getY();
 	    				Line line = new Line();
 	    				line.setStartX(startX);
 	    				line.setStartY(startY);
 	    				line.setEndX(endX);
 	    				line.setEndY(endY);
 	    				anchorPane.getChildren().add(line);
 	    				shapePhil.addDrPhil(line);
 	    				point.setChosen();
 	    				startX = 0; 
 	    				startY = 0;
 	    				endX = 0;
 	    				endY = 0;
 	    				break;
 	    			}
 	    		}
     		}
    	}
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	point = new Point();
    }
}