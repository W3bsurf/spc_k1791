package viikko11_5;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;


public class Point {
	protected String name;
	protected double x1;
	protected boolean chosen;
	protected double y1;
	protected double x2;
	protected double y2;
	protected double x3;
	protected double y3;
	
	
	public Point() {
	}
	
	public Circle createCircle(double x, double y) {
		Circle circ = new Circle();
		chosen = false;
	    circ.setFill(Color.BLUE);
	    circ.setCenterX(x); 
	    circ.setCenterY(y);
	    circ.setRadius(30);
	    circ.setOnMouseClicked(event -> {
	    	System.out.println("Hei olen piste!");
	    	if (chosen == true) {
	    		chosen = false;
	    	} else {
	    		chosen = true;
	    		x1 = circ.getCenterX();
	    		y1 = circ.getCenterY();  
	    	}
	    });
	    return circ;
	}
	
	public double getX() {
		return x1;
	}
	
	public double getY() {
		return y1;
	}
	
	public boolean getChosen() {
		return chosen;
	}
	
	public void setChosen() {
		chosen = false;
	}
}
