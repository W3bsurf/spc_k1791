package viikko4_5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ReadAndWriteIO {
	
	public void readZip(String tiedosto) {
		try {
			ZipFile zipTiedosto = new ZipFile(tiedosto);
			Enumeration<? extends ZipEntry> entries = zipTiedosto.entries();
			BufferedReader in;
			
			while (entries.hasMoreElements()) {
				ZipEntry zipAlkio = entries.nextElement();
				String tiedostoNimi = zipAlkio.getName();
				if (tiedostoNimi.endsWith(".txt")) {
					in = new BufferedReader(new InputStreamReader(zipTiedosto.getInputStream(zipAlkio)));
					String inputLine;
					
					while((inputLine = in.readLine()) != null) {
						System.out.println(inputLine);
					}
					in.close();
				}
			}
		zipTiedosto.close();	
		} catch (IOException ex) {
			System.out.println("Tiedostoa ei löytynyt!");
		}
	}
}
