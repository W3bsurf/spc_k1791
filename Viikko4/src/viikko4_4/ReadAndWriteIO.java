package viikko4_4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReadAndWriteIO {
	
	public void readFile(String tiedosto) {
		try {
			BufferedReader in;
			
			in = new BufferedReader(new FileReader(tiedosto));
			String inputLine;
			
			while((inputLine = in.readLine()) != null) {
				System.out.println(inputLine);	
			}
			in.close();
		} catch (IOException ex) {
			System.out.println("Tiedostoa ei löytynyt!");
		}
	}
	
	public void readAndWrite(String tiedosto1, String tiedosto2) {
		try {
			BufferedReader in;
			BufferedWriter out;
			
			in = new BufferedReader(new FileReader(tiedosto1));
			out = new BufferedWriter(new FileWriter(tiedosto2)); 
			String inputLine;
			
			while ((inputLine = in.readLine()) != null) {
				out.write(inputLine);
			}
			in.close();
			out.close();
		} catch (IOException ex) {
			System.out.println("Tiedostoa ei löytynyt!");
		}
	}
	
	public void readAndWriteCheck(String tiedosto1, String tiedosto2) {
		try {
			BufferedReader in;
			BufferedWriter out;
			
			in = new BufferedReader(new FileReader(tiedosto1));
			out = new BufferedWriter(new FileWriter(tiedosto2));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				if (inputLine.trim().length() < 30 && inputLine.trim().length() > 0 && inputLine.indexOf('v') > -1) {
					out.write(inputLine + "\n");
				}
			}
			in.close();
			out.close();
		} catch (IOException ex) {
			System.out.println("Tiedostoa ei löytynyt!");
		}
	}
}
