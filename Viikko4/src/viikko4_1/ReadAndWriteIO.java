package viikko4_1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadAndWriteIO {
	
	public void readFile(String tiedosto) {
		try {
			BufferedReader in;
			
			in = new BufferedReader(new FileReader("input.txt"));
			String inputLine;
			
			while((inputLine = in.readLine()) != null) {
				System.out.println(inputLine);	
			}
			in.close();
		} catch (IOException ex) {
			System.out.println("Tiedostoa ei löytynyt!");
		}
	}
}


