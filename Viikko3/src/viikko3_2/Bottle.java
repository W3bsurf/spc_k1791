package viikko3_2;

public class Bottle {
	
	public String nimi;
	public String neste;
	public double koko;
	
	public Bottle() {
		nimi = "Pepsi";
		neste = "Pepsi Max";
		koko = 0.3;
	}
	
	public String getName() {
		return neste;
	}

}
