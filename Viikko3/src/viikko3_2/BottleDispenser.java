package viikko3_2;

public class BottleDispenser {
	
	private int bottles;
	private int money;
	
	public BottleDispenser() {
		bottles = 5;
		money = 0;
		
		Bottle[] bottle_array = new Bottle[bottles];
			for(int i = 0; i < bottles; i++) {
				bottle_array[i] = new Bottle();
			}
	}
	
	public void addMoney() {
		money += 1;
		System.out.println("Klink! Lisää rahaa laitteeseen!");
	}
	
	public void buyBottle() {
		Bottle pullo = new Bottle();
		if (money < 1) {
			System.out.println("Syötä rahaa ensin!");
		}
		else {
			if (bottles == 0) {
				System.out.println("Pullot on loppu!");
			}
			else {
				bottles -= 1;
				money -= 1;
				System.out.println("KACHUNK! " + pullo.getName() + " tipahti masiinasta!");
			}
		}
	}
	
	public void returnMoney() {
		money = 0;
		System.out.println("Klink klink. Sinne menivät rahat!");
	}
	
}
