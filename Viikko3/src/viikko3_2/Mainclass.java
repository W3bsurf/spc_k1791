package viikko3_2;

public class Mainclass {
	
	public static void main(String[] args) {
		BottleDispenser pulloMaatti1 = new BottleDispenser();
		pulloMaatti1.addMoney();
		pulloMaatti1.buyBottle();
		pulloMaatti1.buyBottle();
		pulloMaatti1.addMoney();
		pulloMaatti1.addMoney();
		pulloMaatti1.buyBottle();
		pulloMaatti1.returnMoney();
	}

}
