package viikko3_1;

public class BottleDispenser {
	
	private int bottles;
	private int money;
	
	public BottleDispenser() {
		bottles = 5;
		money = 0;
	}
	
	public void addMoney() {
		money += 1;
		System.out.println("Klink! Lisää rahaa laitteeseen!");
	}
	
	public void buyBottle() {
		if (money < 1) {
			System.out.println("Syötä rahaa ensin!");
		}
		else {
			if (bottles == 0) {
				System.out.println("Pullot on loppu!");
			}
			else {
				bottles -= 1;
				money -= 1;
				System.out.println("KACHUNK! Pullo tipahti masiinasta!");
			}
		}
	}
	
	public void returnMoney() {
		money = 0;
		System.out.println("Klink klink. Sinne menivät rahat!");
	}
	
}
