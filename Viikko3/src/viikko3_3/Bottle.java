package viikko3_3;

public class Bottle {
	public String nimi;
	public String neste;
	public double energia;
	public double koko;
	public double hinta;
	
	public Bottle() {
		nimi = "Pepsi";
		neste = "Pepsi Max";
		energia = 0.3;
		koko = 0.5;
		hinta = 1.80;
	}
	
	public String getName() {
		return neste;
	}
	
	public double getPrice() {
		return hinta;
	}

}
