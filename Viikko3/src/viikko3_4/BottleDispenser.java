package viikko3_4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class BottleDispenser {
		
	private int bottles;
	private int money;
	private ArrayList <Bottle> pLista;
	
	public BottleDispenser() {
		bottles = 6;
		money = 0;
	
		pLista = new ArrayList <Bottle> ();
		for (int i = 0; i < bottles; i++) {
			pLista.add(new Bottle());
		}
	}

	public void addMoney() {
		money += 1;
		System.out.println("Klink! Lisää rahaa laitteeseen!");
	}
	
	public void buyBottle() {
		
		int x;
		Bottle pullo = new Bottle();
		if (money < pullo.getPrice()) {
			System.out.println("Syötä rahaa ensin!");
		}
		else {
			if (pLista.isEmpty() == true) {
				System.out.println("Pullot on loppu!");
			}
			else {
				x = pLista.size();
				pLista.remove(x - 1);
				money -= pullo.getPrice();
				System.out.println("KACHUNK! " + pullo.getName() + " tipahti masiinasta!");
			}
		}
	}
	
	public void returnMoney() {
		money = 0;
		System.out.println("Klink klink. Sinne menivät rahat!");
	}
	
	public void listBottles() {
		int x = 0;
		for (int i = 0; i < pLista.size(); i++) {
			x++;
			System.out.println(x + ". Nimi:" + pLista.get(i).getName() + "\n\t Koko: " + pLista.get(i).getSize() + "\tHinta: " + pLista.get(i).getPrice());
		}
	}
	
	public int valikko() {
		int valinta;
		while (true) {
			System.out.print("*** LIMSA-AUTOMAATTI ***\n1) Lisää rahaa koneeseen\n2) Osta pullo\n3) Ota rahat ulos\n4) Listaa koneessa olevat pullot\n0) Lopeta\n");
			System.out.print("Valintasi: ");
			BufferedReader br = new BufferedReader(
					new InputStreamReader(System.in));
			try {
				try {
					valinta = Integer.parseInt(br.readLine());
					if (valinta < 0 || valinta > 4) {
						System.out.println("Valitse 0-4");
					}
					else {
						return valinta;
					}
				} catch (NumberFormatException ex) {
					System.out.println("Valitse 0-4");
				}
			} catch (IOException ex) {
			}
		}
	}		
}
	


