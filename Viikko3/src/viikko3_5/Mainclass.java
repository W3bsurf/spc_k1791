package viikko3_5;

public class Mainclass {
	
	public static void main(String[] args) {
		BottleDispenser pulloMaatti = new BottleDispenser();
		int valinta;
		while (true) {
			valinta = pulloMaatti.valikko();
			if (valinta == 1) {
				pulloMaatti.addMoney();
			}
			else if (valinta == 2) {
				pulloMaatti.buyBottle();
			}
			else if (valinta == 3) {
				pulloMaatti.returnMoney();
			}
			else if (valinta == 4) {
				pulloMaatti.listBottles();
			}
			else if (valinta == 0) {
				break;
			}
		}
	}
}

